From 783bb4e1ec594febf8e44274d7878002ab32f247 Mon Sep 17 00:00:00 2001
From: Mike Skec <skec@protonmail.ch>
Date: Sat, 12 Mar 2022 09:02:01 +1100
Subject: [PATCH] apply various upstream patches for better LibreSSL support.

Merges the following commits from upstream (these should be present in
the next release of Net-SSLeay):
 Use X509_get0_tbs_sigalg() for LibreSSL. (#360)
  https://github.com/radiator-software/p5-net-ssleay/commit/4a886e06c1cac80e7fb3f8d52146a27ce557ba8c
 Use OCSP_SINGLERESP_get0_id() for LibreSSL. (#362) 
  https://github.com/radiator-software/p5-net-ssleay/commit/88c3bbc45399c8ef2c8879aada8bfa91d8bc6c10
 Implement RSA_get_key_parameters() for newer LibreSSL. (#363) 
  https://github.com/radiator-software/p5-net-ssleay/commit/3dd2f101b8e15a59f66e22525b8d001d5ad6ce7d
 Enable SSL get proto version for LibreSSL. (#364)
  https://github.com/radiator-software/p5-net-ssleay/commit/6c5da5b3a4b48d365adc4aca2fbb043416b3b288
 GH-369 Make RSA_get_key_parameters available with OpenSSL 1.1.0 and later.
  https://github.com/radiator-software/p5-net-ssleay/commit/7ff8f49b07301a5c804e3ac69dc3079200ac09a4
---
 SSLeay.xs                     | 51 +++++++++++++++++++++++++++--------
 t/local/33_x509_create_cert.t | 26 +++++++++++++-----
 typemap                       |  1 +
 3 files changed, 60 insertions(+), 18 deletions(-)

diff --git a/SSLeay.xs b/SSLeay.xs
index bfd6320..14c39ee 100644
--- a/SSLeay.xs
+++ b/SSLeay.xs
@@ -1759,10 +1759,10 @@ X509 * find_issuer(X509 *cert,X509_STORE *store, STACK_OF(X509) *chain) {
     return issuer;
 }
 
-SV* bn2sv(BIGNUM* p_bn)
+static SV *bn2sv(const BIGNUM* p_bn)
 {
     return p_bn != NULL
-        ? sv_2mortal(newSViv((IV) BN_dup(p_bn)))
+        ? sv_2mortal(newSViv(PTR2IV(BN_dup(p_bn))))
         : &PL_sv_undef;
 }
 
@@ -4884,7 +4884,7 @@ SSL_set_max_proto_version(ssl, version)
 #endif /* OpenSSL 1.1.0-pre2 or LibreSSL 2.6.0 */
 
 
-#if OPENSSL_VERSION_NUMBER >= 0x1010007fL && !defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER >= 0x1010007fL && !defined(LIBRESSL_VERSION_NUMBER)) || (LIBRESSL_VERSION_NUMBER >= 0x3040000fL)
 
 int
 SSL_CTX_get_min_proto_version(ctx)
@@ -4902,7 +4902,7 @@ int
 SSL_get_max_proto_version(ssl)
      SSL *  ssl
 
-#endif /* OpenSSL 1.1.0g */
+#endif /* OpenSSL 1.1.0g or LibreSSL 3.4.0 */
 
 
 #if OPENSSL_VERSION_NUMBER < 0x10000000L
@@ -5922,6 +5922,18 @@ SSL_set_tmp_rsa(ssl,rsa)
 
 #endif
 
+BIGNUM *
+BN_dup(const BIGNUM *from)
+
+void
+BN_clear(BIGNUM *bn)
+
+void
+BN_clear_free(BIGNUM *bn)
+
+void
+BN_free(BIGNUM *bn)
+
 #if OPENSSL_VERSION_NUMBER >= 0x0090800fL
 
 RSA *
@@ -6001,13 +6013,31 @@ RSA_generate_key(bits,e,perl_cb=&PL_sv_undef,perl_data=&PL_sv_undef)
 
 #endif
 
-#if OPENSSL_VERSION_NUMBER < 0x10100000L || defined(LIBRESSL_VERSION_NUMBER)
-
 void
 RSA_get_key_parameters(rsa)
 	    RSA * rsa
+PREINIT:
+#if (!defined(LIBRESSL_VERSION_NUMBER) && (OPENSSL_VERSION_NUMBER >= 0x1010000fL)) || (defined(LIBRESSL_VERSION_NUMBER) && (LIBRESSL_VERSION_NUMBER >= 0x3050000fL))
+    const BIGNUM *n, *e, *d;
+    const BIGNUM *p, *q;
+    const BIGNUM *dmp1, *dmq1, *iqmp;
+#endif
 PPCODE:
 {
+#if (!defined(LIBRESSL_VERSION_NUMBER) && (OPENSSL_VERSION_NUMBER >= 0x1010000fL)) || (defined(LIBRESSL_VERSION_NUMBER) && (LIBRESSL_VERSION_NUMBER >= 0x3050000fL))
+    RSA_get0_key(rsa, &n, &e, &d);
+    RSA_get0_factors(rsa, &p, &q);
+    RSA_get0_crt_params(rsa, &dmp1, &dmq1, &iqmp);
+    /* Caution: returned list consists of SV pointers to BIGNUMs, which would need to be blessed as Crypt::OpenSSL::Bignum for further use */
+    XPUSHs(bn2sv(n));
+    XPUSHs(bn2sv(e));
+    XPUSHs(bn2sv(d));
+    XPUSHs(bn2sv(p));
+    XPUSHs(bn2sv(q));
+    XPUSHs(bn2sv(dmp1));
+    XPUSHs(bn2sv(dmq1));
+    XPUSHs(bn2sv(iqmp));
+#else
     /* Caution: returned list consists of SV pointers to BIGNUMs, which would need to be blessed as Crypt::OpenSSL::Bignum for further use */
     XPUSHs(bn2sv(rsa->n));
     XPUSHs(bn2sv(rsa->e));
@@ -6017,9 +6047,8 @@ PPCODE:
     XPUSHs(bn2sv(rsa->dmp1));
     XPUSHs(bn2sv(rsa->dmq1));
     XPUSHs(bn2sv(rsa->iqmp));
-}
-
 #endif
+}
 
 void
 RSA_free(r)
@@ -6920,7 +6949,7 @@ ASN1_OBJECT *
 P_X509_get_signature_alg(x)
         X509 * x
     CODE:
-#if OPENSSL_VERSION_NUMBER >= 0x10100000L && !defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER >= 0x10100000L && !defined(LIBRESSL_VERSION_NUMBER)) || (LIBRESSL_VERSION_NUMBER >= 0x3050000fL)
         RETVAL = (X509_get0_tbs_sigalg(x)->algorithm);
 #else
         RETVAL = (x->cert_info->signature->algorithm);
@@ -7412,7 +7441,7 @@ OCSP_response_results(rsp,...)
 		if (!idsv) {
 		    /* getall: create new SV with OCSP_CERTID */
 		    unsigned char *pi,*pc;
-#if OPENSSL_VERSION_NUMBER >= 0x10100003L && !defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER >= 0x10100003L && !defined(LIBRESSL_VERSION_NUMBER)) || (LIBRESSL_VERSION_NUMBER >= 0x3050000fL)
 		    int len = i2d_OCSP_CERTID(OCSP_SINGLERESP_get0_id(sir),NULL);
 #else
 		    int len = i2d_OCSP_CERTID(sir->certId,NULL);
@@ -7421,7 +7450,7 @@ OCSP_response_results(rsp,...)
 		    Newx(pc,len,unsigned char);
 		    if (!pc) croak("out of memory");
 		    pi = pc;
-#if OPENSSL_VERSION_NUMBER >= 0x10100003L && !defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER >= 0x10100003L && !defined(LIBRESSL_VERSION_NUMBER)) || (LIBRESSL_VERSION_NUMBER >= 0x3050000fL)
 		    i2d_OCSP_CERTID(OCSP_SINGLERESP_get0_id(sir),&pi);
 #else
 		    i2d_OCSP_CERTID(sir->certId,&pi);
diff --git a/t/local/33_x509_create_cert.t b/t/local/33_x509_create_cert.t
index 3c5a2e8..3666095 100755
--- a/t/local/33_x509_create_cert.t
+++ b/t/local/33_x509_create_cert.t
@@ -5,7 +5,7 @@ use Test::Net::SSLeay qw( data_file_path initialise_libssl is_openssl );
 
 use utf8;
 
-plan tests => 139;
+plan tests => 141;
 
 initialise_libssl();
 
@@ -27,12 +27,24 @@ is(Net::SSLeay::X509_NAME_cmp($ca_issuer, $ca_subject), 0, "X509_NAME_cmp");
   ok(my $rsa = Net::SSLeay::RSA_generate_key(2048, &Net::SSLeay::RSA_F4), "RSA_generate_key");
   ok(Net::SSLeay::EVP_PKEY_assign_RSA($pk,$rsa), "EVP_PKEY_assign_RSA");
 
-  SKIP: 
-  {
-    skip 'openssl<1.1.0 required', 1 unless Net::SSLeay::SSLeay < 0x10100000
-       or Net::SSLeay::constant("LIBRESSL_VERSION_NUMBER");
-    my @params = Net::SSLeay::RSA_get_key_parameters($rsa);
-    ok(@params == 8, "RSA_get_key_parameters");
+  my @params = Net::SSLeay::RSA_get_key_parameters($rsa);
+  ok(@params == 8, "RSA_get_key_parameters");
+
+ SKIP: {
+     skip('No Crypt::OpenSSL::Bignum for additional tests', 2)
+	 unless eval {require Crypt::OpenSSL::Bignum; 1; };
+
+     # Check that the exponent is what we expect and that our calls
+     # don't clear and free the original value. See
+     # RSA_get_key_parameters in the manual for the details.
+     my $bn = Net::SSLeay::BN_dup($params[1]);
+     my $r = Crypt::OpenSSL::Bignum->bless_pointer($bn);
+     is($r->to_decimal(), Net::SSLeay::RSA_F4(), 'Crypt::OpenSSL::Bignum exponent once');
+     undef $r;
+
+     $bn = Net::SSLeay::BN_dup($params[1]);
+     $r = Crypt::OpenSSL::Bignum->bless_pointer($bn);
+     is($r->to_decimal(), Net::SSLeay::RSA_F4(), 'Crypt::OpenSSL::Bignum exponent twice');
   }
  
   ok(my $x509  = Net::SSLeay::X509_new(), "X509_new");
diff --git a/typemap b/typemap
index 373e711..dc31c65 100644
--- a/typemap
+++ b/typemap
@@ -23,6 +23,7 @@ X509_NAME_ENTRY *   T_PTR
 X509_EXTENSION *	T_PTR
 X509_REQ *      T_PTR
 X509_PUBKEY *   T_PTR
+const BIGNUM *        T_PTR
 BIGNUM *        T_PTR
 BIO *           T_PTR
 const BIO_METHOD *    T_PTR
-- 
2.34.1

