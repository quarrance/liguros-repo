# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="HTML to text converter"
HOMEPAGE="https://github.com/grobian/html2text"
SRC_URI="https://github.com/grobian/html2textdownloads/${P}.tar.gz
	https://github.com/grobian/html2textdownloads/patch-utf8-${P}.diff
	https://github.com/grobian/html2textdownloads/patch-amd64-${P}.diff
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha amd64 ~hppa ~ia64 ppc ppc64 sparc x86 ~amd64-linux ~x86-linux ~ppc-macos"

PATCHES=(
	"${FILESDIR}/${P}-compiler.patch"
	"${FILESDIR}/${P}-urlistream-get.patch"
	"${DISTDIR}/patch-utf8-${P}.diff"
	"${DISTDIR}/patch-amd64-${P}.diff"
)

src_prepare() {
	default
	gunzip html2text.1.gz html2textrc.5.gz || die
	tc-export CXX
}

src_compile() {
	emake LDFLAGS="${LDFLAGS}" DEBUG="${CXXFLAGS}"
}

src_install() {
	dobin html2text
	doman html2text.1 html2textrc.5
	dodoc CHANGES CREDITS KNOWN_BUGS README TODO
}
