# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit linux-info eutils autotools

DESCRIPTION="Console-based audio visualizer for ALSA"
HOMEPAGE="https://github.com/karlstav/cava"
SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="MIT"
SLOT="0"
IUSE="alsa debug portaudio pulseaudio"

DEPEND="
	dev-libs/iniparser
	sci-libs/fftw:3.0
	sys-libs/ncurses
"
RDEPEND="${DEPEND}
	alsa? ( media-libs/alsa-lib )
	pulseaudio? ( media-sound/pulseaudio )
	portaudio? ( media-libs/portaudio )
"

DOCS=( README.md example_files/ )

CONFIG_CHECK=(
	SND_ALOOP
)

src_prepare() {
	eapply "${FILESDIR}/iniparser.patch"
	eapply_user
	eautoreconf
	${S}/autogen.sh
}

src_configure() {
	econf \
		$(use_enable debug) \
		$(use_enable alsa input-alsa) \
		$(use_enable pulseaudio input-pulse) \
		$(use_enable portaudio input-portaudio) \
		--docdir="${EREFIX}/usr/share/doc/${PF}"
}

src_compile() {
	emake
}

src_install() {
	einstalldocs
	emake DESTDIR="${D}" PREFIX=/usr install
}
