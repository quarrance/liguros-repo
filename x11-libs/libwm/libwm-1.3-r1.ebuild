# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="A small library for X windows manipulation"
HOMEPAGE="https://github.com/wmutils/libwm"
SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="ISC"
SLOT="0"

DEPEND="
	x11-libs/libxcb
	x11-misc/xcb
	x11-libs/xcb-util-cursor
"
RDEPEND="${DEPEND}"

DOCS=( README.md )

src_install() {
	emake install DESTDIR="${D}" PREFIX="/usr"
	einstalldocs
}
