# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1

DESCRIPTION="Text input widget for urwid that supports readline shortcuts."
HOMEPAGE="https://pypi.org/project/urwid-readline/ https://github.com/rr-/urwid_readline"
SRC_URI="https://files.pythonhosted.org/packages/ab/bb/c5b3fec22268d97ad30232f5533d4a5939d4df7ed3917a8d20d447f1d0a7/urwid_readline-0.13.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

src_prepare() {
	distutils-r1_src_prepare
}

python_install_all() {
	distutils-r1_python_install_all
	python_optimize
}
