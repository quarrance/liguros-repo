# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit cmake gnome2-utils xdg

MY_PN="CPU-X"
DESCRIPTION="A Free software that gathers information on CPU, motherboard and more"
HOMEPAGE="https://x0rg.github.io/CPU-X"
SRC_URI="https://github.com/X0rg/${MY_PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="-* amd64 x86"
IUSE="+bandwidth +dmidecode force-libstatgrab +gtk +libcpuid +libpci +ncurses +nls appimage"
RESTRICT="primaryuri"

DEPEND="
	dev-libs/json-c:=
	net-misc/curl
	!force-libstatgrab? ( sys-process/procps:= )
	force-libstatgrab? ( sys-libs/libstatgrab )
	gtk? ( >=x11-libs/gtk+-3.12:3 )
	media-libs/glfw
	libcpuid? ( >=sys-libs/libcpuid-0.5.1:= )
	libpci? ( sys-apps/pciutils )
	ncurses? ( sys-libs/ncurses:= )
	sys-apps/lm-sensors
"

BDEPEND="
	dev-lang/nasm
	nls? ( sys-devel/gettext )
"

RDEPEND="${DEPEND}"

S="${WORKDIR}/${MY_PN}-${PV}"

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DWITH_GTK=$(usex gtk)
		-DWITH_NCURSES=$(usex ncurses)
		-DWITH_GETTEXT=$(usex nls)
		-DWITH_LIBCPUID=$(usex libcpuid)
		-DWITH_LIBPCI=$(usex libpci)
		-DWITH_LIBSTATGRAB=OFF
		-DWITH_DMIDECODE=$(usex dmidecode)
		-DWITH_BANDWIDTH=$(usex bandwidth)
		-DFORCE_LIBSTATGRAB=$(usex force-libstatgrab)
		-DAPPIMAGE=$(usex appimage)
	)

	cmake_src_configure
}

src_install() {
	addpredict "/usr/share/glib-2.0/schemas/"
	cmake_src_install
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_preinst() {
	gnome2_schemas_savelist
}

pkg_postrm() {
	gnome2_schemas_update
}
