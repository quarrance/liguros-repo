# Copyright 2020-2022 by Liguros Authors 
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} pypy3 )

inherit distutils-r1

DESCRIPTION="Python library of web-related functions"
HOMEPAGE="https://github.com/scrapy/w3lib"
SRC_URI="https://github.com/scrapy/w3lib/archive/v${PV}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"

RDEPEND="dev-python/six[${PYTHON_USEDEP}]"

distutils_enable_tests pytest

EPYTEST_DESELECT=(
	# https://github.com/scrapy/w3lib/issues/164
	tests/test_url.py::UrlTests::test_add_or_replace_parameter
)
