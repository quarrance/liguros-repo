# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson

HOMEPAGE="https://www.flameeyes.eu/projects/unpaper"
DESCRIPTION="Post-processor for scanned and photocopied book pages"
KEYWORDS="amd64 ~arm64 x86"
SRC_URI="https://github.com/unpaper/unpaper/archive/refs/tags/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
IUSE="test"

RDEPEND="
	>=media-video/ffmpeg-2:0=[encode]
"
DEPEND="
	dev-libs/libxslt
	app-text/docbook-xsl-ns-stylesheets
	virtual/pkgconfig
	${RDEPEND}
"

S=${WORKDIR}/${PN}-${P}

RESTRICT="test"

src_prepare() {
	default
#	eautoreconf
}
