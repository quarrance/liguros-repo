# Copyright 2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Sixty-six is a collection of system tools built around s6 and s6-rc created to make the implementation and manipulation of service files on your machine easier."
HOMEPAGE="https://framagit.org/Obarun/66"
SRC_URI="https://framagit.org/Obarun/66/-/archive/v${PV}/${PN}-v${PV}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	>=sys-devel/make-3.81
	>=dev-libs/skalibs-2.8.1.0
	>=dev-lang/execline-2.5.1.0
	>=sys-apps/s6-2.8.0.1
	>=sys-apps/s6-rc-0.5.0.0
	>=sys-libs/oblibs-0.0.2.0
	>=app-text/scdoc-1.9.4
"

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
	econf --disable-shared --with-system-service=/usr/lib/66/service --with-sysdeps=/usr/lib/skalibs
}

src_install() {
	emake DESTDIR="${D}" install

	dodoc INSTALL.md NEWS.md README.md AUTHORS
}
