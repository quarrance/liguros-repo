# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1

DESCRIPTION="Sphinx Extension to enable OGP support"
HOMEPAGE="https://github.com/wpilibsuite/sphinxext-opengraph"
SRC_URI="https://files.pythonhosted.org/packages/7d/96/00032892455268fd3748c2bb29357138ec829def3dae96929e7734024e08/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

distutils_enable_sphinx docs
distutils_enable_tests setup.py

