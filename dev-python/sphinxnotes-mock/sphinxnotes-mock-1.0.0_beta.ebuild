# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"
RESTRICT="test"

inherit distutils-r1

DESCRIPTION="Sphinx extension for mocking directives and roles"
HOMEPAGE="https://sphinx.silverrainz.me/mock/"
SRC_URI="https://files.pythonhosted.org/packages/80/90/f2994a98fd262a8d6cb2ed482b83fb1b064aebc54da0b86c219d930a28f0/sphinxnotes-mock-1.0.0b0.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

S=${WORKDIR}/sphinxnotes-mock-1.0.0b0

distutils_enable_sphinx docs
distutils_enable_tests setup.py

