# Copyright 2020-2022 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools bash-completion-r1 linux-info systemd

DESCRIPTION="Fast, dense and secure container management"
HOMEPAGE="https://linuxcontainers.org/lxd/introduction/"
LICENSE="Apache-2.0 BSD BSD-2 LGPL-3 MIT MPL-2.0"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 x86"
IUSE="+daemon +dnsmasq nls test +tools virtual-machine"
SRC_URI="https://linuxcontainers.org/downloads/${PN}/${P}.tar.gz"

MIN_KERN_VER="5.4.0"

DEPEND="
	${RDEPEND}
	acct-group/lxd
	>=dev-libs/raft-0.9.22
	>=dev-db/sqlite-3.25
	dev-libs/dqlite
	dev-lang/tcl
	>=dev-lang/go-1.18.0
	dev-libs/libuv
	dev-libs/protobuf
	nls? ( sys-devel/gettext )
	test? (
		app-misc/jq
		net-misc/curl
		sys-devel/gettext
	)
	virtual-machine? ( >=app-emulation/qemu-6.0:=[virtfs(+)]  )
"

RDEPEND="
	daemon? (
		dev-libs/raft
		app-arch/xz-utils
		>=app-containers/lxc-4.0.0
		dev-libs/libuv
		dev-libs/lzo
		dev-util/xdelta:3
		dnsmasq? (
			net-dns/dnsmasq[dhcp]
		)
		net-firewall/ebtables
		net-firewall/iptables
		net-libs/libnfnetlink
		net-libs/libnsl:0=
		net-misc/rsync[xattr]
		sys-apps/iproute2
		sys-fs/fuse
		sys-fs/lxcfs
		sys-fs/squashfs-tools[lzma]
		virtual/acl
	)
"

CONFIG_CHECK="
	~BRIDGE
	~DUMMY
	~IP6_NF_NAT
	~IP6_NF_TARGET_MASQUERADE
	~IPV6
	~IP_NF_NAT
	~IP_NF_TARGET_MASQUERADE
	~MACVLAN
	~NETFILTER_XT_MATCH_COMMENT
	~NET_IPGRE
	~NET_IPGRE_DEMUX
	~NET_IPIP
	~SCHED_SMT
	~VHOST_VSOCK
	~VXLAN
"

ERROR_BRIDGE="BRIDGE: needed for network commands"
ERROR_DUMMY="DUMMY: needed for network commands"
ERROR_IP6_NF_NAT="IP6_NF_NAT: needed for network commands"
ERROR_IP6_NF_TARGET_MASQUERADE="IP6_NF_TARGET_MASQUERADE: needed for network commands"
ERROR_IPV6="IPV6: needed for network commands"
ERROR_IP_NF_NAT="IP_NF_NAT: needed for network commands"
ERROR_IP_NF_TARGET_MASQUERADE="IP_NF_TARGET_MASQUERADE: needed for network commands"
ERROR_MACVLAN="MACVLAN: needed for network commands"
ERROR_NETFILTER_XT_MATCH_COMMENT="NETFILTER_XT_MATCH_COMMENT: needed for network commands"
ERROR_NET_IPGRE="NET_IPGRE: needed for network commands"
ERROR_NET_IPGRE_DEMUX="NET_IPGRE_DEMUX: needed for network commands"
ERROR_NET_IPIP="NET_IPIP: needed for network commands"
ERROR_SCHED_SMT="SCHED_SMT: needed for core scheduling"
ERROR_VHOST_VSOCK="VHOST_VSOCK: needed for network commands"
ERROR_VXLAN="VXLAN: needed for network commands"

EGO_PN="github.com/lxc/lxd"

src_prepare() {
	# check for some kernel parameter depending on kernel version
	if kernel_is lt 5 1; then
		CONFIG_CHECK="~NF_NAT_IPV4 ~NF_NAT_MASQUERADE_IPV4"
		CONFIG_CHECK="~NF_NAT_IPV6 ~NF_NAT_MASQUERADE_IPV6"
	else
		CONFIG_CHECK="~NF_NAT ~NF_NAT_MASQUERADE"
	fi

	# Check for core scheduling
	if kernel_is ge 5 14; then
		CONFIG_CHECK="~SCHED_CORE"
	fi
	linux-info_pkg_setup

	default
	mkdir ${S}/bin
}

src_compile() {
	export GOFLAGS="-buildmode=pie -trimpath"
	export CGO_LDFLAGS_ALLOW="(-Wl,-wrap,pthread_create)|(-Wl,-z,now)"

	cd "${S}"
	go build -v -tags "netgo" -o bin/ ./lxd-migrate/...
	CGO_LDFLAGS="$CGO_LDFLAGS -static" go build -v -tags "agent" -o bin/ ./lxd-agent/...
	go build -v -x -o bin/ ./lxc || die "Failed to build the client"

	if use daemon; then
		go build -v -x -o bin/ ./lxd || die "Failed to build the daemon"
	fi

	if use tools; then
		for tool in fuidshift lxc lxd lxc-to-lxd lxd-agent lxd-benchmark lxd-migrate; do
			go build -v -x -o bin/ ./${tool} || die "Failed to build ${tool}"
		done
	fi

	use nls && emake build-mo

	mkdir man
	./bin/lxc manpage man/
}

src_test() {
	if use daemon; then
		go get -v -x github.com/rogpeppe/godeps
		go get -v -x github.com/remyoudompheng/go-misc/deadcode
		go get -v -x github.com/golang/lint/golint
		go test -v ${EGO_PN}/lxd
	else
		einfo "No tests to run for client-only builds"
	fi
}

src_install() {
	local bindir="bin"
	dobin ${bindir}/lxc

	if use daemon; then
		cd "${S}" || die "Can't cd to \${S}"
		dosbin ${bindir}/lxd
	fi

	if use tools; then
		dobin ${bindir}/fuidshift
		dobin ${bindir}/lxc-to-lxd
		dobin ${bindir}/lxd-agent
		dobin ${bindir}/lxd-benchmark
		dobin ${bindir}/lxd-migrate
	fi

	insinto /etc/sysctl.d
	newins "${FILESDIR}/${PN}-sysctl.conf" 60-${PN}.conf

	if use nls; then
		domo po/*.mo
	fi

	if use daemon; then
		newinitd "${FILESDIR}"/${PN}.initd lxd
		newconfd "${FILESDIR}"/${PN}.confd lxd

		systemd_newunit "${FILESDIR}"/${PN}.service ${PN}.service
	fi

	newbashcomp scripts/bash/lxd-client lxc

	doman man/*
	dodoc AUTHORS doc/*md
}

pkg_postinst() {
	elog
	elog "Consult https://wiki.gentoo.org/wiki/LXD for more information,"
	elog "including a Quick Start."

	# The messaging below only applies to daemon installs
	use daemon || return 0

	# Ubuntu also defines an lxd user but it appears unused (the daemon
	# must run as root)
	elog
	elog "Though not strictly required, some features are enabled at run-time"
	elog "when the relevant helper programs are detected:"
	elog "- sys-apps/apparmor"
	elog "- sys-fs/btrfs-progs"
	elog "- sys-fs/lvm2"
	elog "- sys-fs/zfs"
	elog "- sys-process/criu"
	elog
	elog "Since these features can't be disabled at build-time they are"
	elog "not USE-conditional."
	elog
	elog "Be sure to add your local user to the lxd group."
	elog
	elog "Networks with bridge.mode=fan are unsupported due to requiring"
	elog "a patched kernel and iproute2."
}
