# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit distutils-r1

DESCRIPTION="A tool and python library that helps when interfacing with Ansible"
HOMEPAGE="https://github.com/ansible/ansible-runner"
SRC_URI="
	https://github.com/ansible/ansible-runner/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	dev-python/pexpect[${PYTHON_USEDEP}]
	dev-python/packaging[${PYTHON_USEDEP}]
	dev-python/psutil[${PYTHON_USEDEP}]
	dev-python/python-daemon[${PYTHON_USEDEP}]
	dev-python/pyyaml[${PYTHON_USEDEP}]
	dev-python/setuptools[${PYTHON_USEDEP}]
	dev-python/six[${PYTHON_USEDEP}]
"
BDEPEND="
	test? (
		dev-python/mock[${PYTHON_USEDEP}]
	)
"

distutils_enable_tests pytest

python_prepare_all() {
	sed -e 's|find_packages()|find_packages(exclude=["test", "test.*"])|' -i setup.py || die
	git init -q || die
	git add . || die
	git config user.email "dev@liguros.net" || die
	git config user.name "Liguros development" || die
	git commit -m "init" || die
	git tag -a "${PV}" -m "${PV}" || die

	distutils-r1_python_prepare_all
}
