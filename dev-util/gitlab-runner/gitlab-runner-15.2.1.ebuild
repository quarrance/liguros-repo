# Copyright 2020-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module bash-completion-r1 readme.gentoo-r1 systemd

DESCRIPTION="GitLab Runner"
HOMEPAGE="https://gitlab.com/gitlab-org/gitlab-runner"

SRC_URI="
	${HOMEPAGE}/-/archive/v${PV}/${PN}-v${PV}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm ~arm64"
IUSE=""

RDEPEND="
	acct-group/gitlab
	acct-user/gitlab-runner
	>=dev-lang/go-1.17.7
"
DEPEND="${RDEPEND}"

RESTRICT="mirror strip"

DOC_CONTENTS="Register the runner as root using\\n
\\t# gitlab-runner register\\n
Configure the runner in /etc/gitlab-runner/config.toml"

S="${WORKDIR}/${PN}-v${PV}"

src_compile() {
	LDFLAGS="-X ${NS}.NAME=${PN} -X ${NS}.VERSION=${PV}
		-X ${NS}.REVISION=${REVISION} -X ${NS}.BUILT=$(date -u +%Y-%m-%dT%H:%M:%S%z)
		-X ${NS}.BRANCH=${BRANCH}"
	go build -o ${PN} -ldflags "${LDFLAGS}"
}

src_install() {
	einstalldocs

	exeinto /usr/libexec/gitlab-runner
	doexe gitlab-runner
	dosym ../libexec/gitlab-runner/gitlab-runner /usr/bin/gitlab-runner

	newconfd "${FILESDIR}"/gitlab-runner.confd gitlab-runner
	newinitd "${FILESDIR}"/gitlab-runner.initd gitlab-runner
	systemd_dounit "${FILESDIR}"/gitlab-runner.service

	readme.gentoo_create_doc

	insopts -oroot -ggitlab -m0640
	diropts -oroot -ggitlab -m0750
	insinto /etc/gitlab-runner
	keepdir /etc/gitlab-runner /var/lib/gitlab-runner
	doins "${S}"/config.toml.example
}

pkg_postinst() {
	readme.gentoo_print_elog
}
