# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1
SRC_URI="https://files.pythonhosted.org/packages/21/01/46cc2c73b92c218af7a61dde05a8c17c21d6cc45e1fbc695091faf550fb0/${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

DESCRIPTION="Stress-Terminal UI monitoring tool"
HOMEPAGE="https://amanusk.github.io/s-tui"

LICENSE="GPL-2"
SLOT="0"
IUSE="stress"

RDEPEND="
	dev-python/urwid[${PYTHON_USEDEP}]
	dev-python/psutil[${PYTHON_USEDEP}]
	stress? ( app-benchmarks/stress )
"

DEPEND="${RDEPEND}"

BDEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
"
