# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="C++ wrapper around the public domain SQLite 3.x database"
HOMEPAGE="https://utelle.github.io/wxsqlite3/docs/html/index.html"
SRC_URI="https://github.com/utelle/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="wxWinLL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="doc"

DEPEND="
	x11-libs/wxGTK:3.0[X]
	dev-db/sqlite:3
	doc? (
		app-doc/doxygen[dot]
	)"
RDEPEND="${DEPEND}"

DOCS=(readme.md COPYING.txt GPL-3.0.txt LGPL-3.0.txt LICENCE.txt )

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	econf --prefix="${EPREFIX}/usr" --enable-shared --with-wx-config="${WX_CONFIG}"
	default
}

src_compile() {
	default

	if use doc; then
		pushd docs
		doxygen Doxyfile || die
		popd
	fi
}

src_install() {
	default
	insinto /usr/$(get_libdir)/pkgconfig
	doins ${PN}.pc

	if use doc; then
		HTML_DOCS=( docs/html/* )
		einstalldocs
    fi
}
