# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="RFC3261 compliant SIP User-Agent library"
HOMEPAGE="https://github.com/freeswitch/sofia-sip"
SRC_URI="${HOMEPAGE}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1+ BSD public-domain" # See COPYRIGHT
SLOT="0"
KEYWORDS="~alpha amd64 ~arm arm64 ~ia64 ppc ~ppc64 sparc x86 ~x86-linux"
IUSE="libressl ssl"
# tests are broken, see bugs 304607 and 330261
RESTRICT="test"

RDEPEND="
	dev-libs/glib:2
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
	)"
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"

src_prepare() {
	${S}/autogen.sh
	default
}

src_configure() {
	econf \
		--disable-static \
		$(use_with ssl openssl)
}

src_install() {
	default
	dodoc RELEASE

	# no static archives
	find "${D}" -name '*.la' -delete || die
}
