# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1

DESCRIPTION="Sphinx extension to render global site tag of Google."
HOMEPAGE="https://github.com/attakei/sphinxcontrib-gtagjs"
SRC_URI="https://files.pythonhosted.org/packages/5a/ff/5b8efed1d848a40ecb8f191fd031282a990bc85ff18f4cc46b21d0ceb7b9/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

distutils_enable_sphinx docs
distutils_enable_tests setup.py

