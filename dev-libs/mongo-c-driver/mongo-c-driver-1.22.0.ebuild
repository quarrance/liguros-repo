# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake

DESCRIPTION="Client library written in C for MongoDB"
HOMEPAGE="https://github.com/mongodb/mongo-c-driver"
SRC_URI="https://github.com/mongodb/mongo-c-driver/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~hppa ~s390 ~x86"
IUSE="debug doc examples icu libressl sasl ssl test"

# No tests on x86 because tests require dev-db/mongodb which don't support
# x86 anymore (bug #645994)
RESTRICT="x86? ( test )
	!test? ( test )"

RDEPEND="app-arch/snappy:=
	app-arch/zstd:=
	sys-libs/zlib:=
	icu? ( dev-libs/icu:= )
	sasl? ( dev-libs/cyrus-sasl:= )
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
	)"
DEPEND="${RDEPEND}
	dev-libs/libbson[static-libs]
	test? (
		dev-db/mongodb
	)"
BDEPEND="doc? ( dev-python/sphinx:= )"

PATCHES=(
	${FILESDIR}/calc_release_version_py.patch
	${FILESDIR}/CMakelist_txt.patch
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_SKIP_RPATH=ON # mongoc-stat insecure runpath
		-DENABLE_BSON=SYSTEM
		-DENABLE_AUTOMATIC_INIT_AND_CLEANUP=OFF
		-DCMAKE_BUILD_TYPE=Release
		-DENABLE_EXAMPLES="$(usex examples ON OFF)"
		-DENABLE_ICU="$(usex icu ON OFF)"
		-DENABLE_HTML_DOCS="$(usex doc ON OFF)"
		-DENABLE_MAN_PAGES="$(usex doc ON OFF)"
		-DENABLE_MONGOC=ON
		-DENABLE_PIC=ON
		-DENABLE_SNAPPY=SYSTEM
		-DENABLE_ZLIB=SYSTEM
		-DENABLE_SASL="$(usex sasl CYRUS OFF)"
		-DENABLE_SSL="$(usex ssl $(usex libressl LIBRESSL OPENSSL) OFF)"
		-DENABLE_STATIC=ON
		-DENABLE_TESTS="$(usex test ON OFF)"
		-DENABLE_TRACING="$(usex debug ON OFF)"
		-DENABLE_DEBUG_ASSERTIONS="$(usex debug ON OFF)"
		-DENABLE_UNINSTALL=OFF
		-DENABLE_ZSTD=ON
	)

	cmake_src_configure
}

src_test() {
	local PORT=27099
	mongod --port ${PORT} --bind_ip 127.0.0.1 --nounixsocket --fork \
		--dbpath="${T}" --logpath="${T}/mongod.log" || die
	MONGOC_TEST_URI="mongodb://[127.0.0.1]:${PORT}" ../mongo-c-driver-${PV}_build/src/libmongoc/test-libmongoc || die
	kill $(<"${T}/mongod.lock")
}

src_install() {
	if use examples; then
		docinto examples
		dodoc src/libmongoc/examples/*.c
	fi

	cmake_src_install
}
