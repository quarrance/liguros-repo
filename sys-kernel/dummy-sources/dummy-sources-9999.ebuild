# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A dummy package for the Linux kernel sources. Ideal for those who like to pull from kernel.org and manage themselves."
HOMEPAGE="https://kernel.org"
SRC_URI=""

KEYWORDS="*"
LICENSE="GPL-2"

SLOT="0"

IUSE=""

PDEPEND="virtual/linux-sources"
