# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake xdg

DESCRIPTION="Mumble is an open source, low-latency, high quality voice chat software"
HOMEPAGE="https://wiki.mumble.info"
EGIT_REPO_URI="https://github.com/mumble-voip/mumble.git"
EGIT_SUBMODULES=( '*' -3rdparty/mach-override-src -3rdparty/minhook -opus -speex -3rdparty/speexdsp )

if [[ "${PV}" == *_pre* ]] ; then
	SRC_URI="https://dev.gentoo.org/~polynomial-c/dist/${P}.tar.xz"
else
	MY_PV="${PV/_/-}"
	MY_P="${PN}-${MY_PV}"
	SRC_URI="https://dl.mumble.info/stable/${MY_P}.tar.gz"
	S="${WORKDIR}/${P}.src"
fi

KEYWORDS="~amd64 ~x86"
LICENSE="BSD MIT"
SLOT="0"
IUSE="+alsa +dbus debug g15 jack libressl multilib pipewire portaudio pulseaudio nls +rnnoise speech +system-rnnoise test zeroconf"
RESTRICT="!test? ( test )"

RDEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5[ssl]
	dev-qt/qtsql:5[sqlite]
	dev-qt/qtsvg:5
	dev-qt/qtwidgets:5
	dev-qt/qtxml:5
	dev-qt/qtconcurrent:5
	>=dev-libs/protobuf-2.2.0:=
	dev-libs/poco[util,xml,zip]
	>=media-libs/libsndfile-1.0.20[-minimal]
	>=media-libs/opus-1.3.1
	>=media-libs/speex-1.2.0
	media-libs/speexdsp
	sys-apps/lsb-release
	x11-libs/libX11
	x11-libs/libXi
	alsa? ( media-libs/alsa-lib )
	dbus? ( dev-qt/qtdbus:5 )
	g15? ( app-misc/g15daemon:= )
	jack? ( virtual/jack )
	!libressl? ( >=dev-libs/openssl-1.0.0b:0= )
	libressl? ( dev-libs/libressl )
	portaudio? ( media-libs/portaudio )
	pulseaudio? ( media-sound/pulseaudio )
	pipewire? ( media-video/pipewire )
	speech? ( >=app-accessibility/speech-dispatcher-0.8.0 )
	system-rnnoise? ( >=media-libs/rnnoise-0.4.1_p20210122 )
	zeroconf? ( net-dns/avahi[mdnsresponder-compat] )
"

DEPEND="${RDEPEND}
	>=dev-libs/boost-1.41.0
	x11-base/xorg-proto
"

BDEPEND="
	dev-qt/linguist-tools:5
	test? ( dev-qt/qttest:5 )
	virtual/pkgconfig
"

PATCHES=(
	"${FILESDIR}/${PN}-1.4.230-gcc12-include-memory.patch"
	"${FILESDIR}/${PN}-1.4.230-poco-link-cmake.patch"
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {

	local mycmakeargs=(
		-Dalsa="$(usex alsa)"
		-Dtests="$(usex test)"
		-Dbundled-celt="ON"
		-Dbundled-opus="OFF"
		-Dbundled-speex="OFF"
		-Ddbus="$(usex dbus)"
		-Dg15="$(usex g15)"
		-Djackaudio="$(usex jack)"
		-Doverlay="ON"
		-Dportaudio="$(usex portaudio)"
		-Dpipewire="$(usex pipewire)"
		-Dpulseaudio="$(usex pulseaudio)"
		-Drnnoise="$(usex rnnoise)"
		-Dserver="OFF"
		-Dspeechd="$(usex speech)"
		-Dbundled-rnnoise=$(usex !system-rnnoise)
		-Dtranslations="$(usex nls)"
		-Dupdate="OFF"
		-Dzeroconf="$(usex zeroconf)"
		-Dwarnings-as-errors="OFF"
		-Doverlay-xcompile="$(usex multilib)"
	)
	if [ -z "${_GIT_R3}" ]; then
		mycmakeargs+=( -DBUILD_NUMBER=$(ver_cut 3) )
	fi

	cmake_src_configure
}

src_install() {
	cmake_src_install

	if use amd64 && use multilib ; then
		# The 32bit overlay library gets built when multilib is enabled.
		# Install it into the correct 32bit lib dir.
		local libdir_64="/usr/$(get_libdir)/mumble"
		local libdir_32="/usr/$(get_abi_var LIBDIR x86)/mumble"
		dodir ${libdir_32}
		mv "${ED}"/${libdir_64}/libmumbleoverlay.x86.so* \
			"${ED}"/${libdir_32}/ || die
	fi
}

pkg_postinst() {
	xdg_pkg_postinst
	echo
	elog "Visit https://wiki.mumble.info/ for futher configuration instructions."
	elog "Run 'mumble-overlay <program>' to start the OpenGL overlay (after starting mumble)."
	echo
}
