# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Matrix group messaging app"
HOMEPAGE="https://wiki.gnome.org/Apps/Fractal"
RESTRICT="network-sandbox"
COMMIT_ID="d1cbba5130bf1f13695e65c040ef85c8a86bef7e"

inherit meson gnome2-utils xdg-utils

# fixed source URL to update the libhandy dependency
SRC_URI="https://gitlab.gnome.org/GNOME/fractal/-/archive/${COMMIT_ID}/fractal-${COMMIT_ID}.tar.gz"
KEYWORDS="~amd64 ~x86"
LICENSE="GPL-3"
SLOT="0"
IUSE="libressl"
S="${WORKDIR}/fractal-${COMMIT_ID}"
RDEPEND=">=virtual/rust-1.31.1
	>=app-text/gspell-1.8.1
	libressl? ( <=dev-libs/libressl-3.3.1 )
	!libressl? ( dev-libs/openssl:0= )
	>=gui-libs/libhandy-1.0.0
	media-libs/gstreamer-editing-services
	gnome-base/gnome-keyring
	>=x11-libs/cairo-1.16.0
	x11-libs/gtksourceview:4"
DEPEND="${RDEPEND}"
BDEPEND="dev-util/ninja
	dev-util/meson"

pkg_preinst() {
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_icon_cache_update
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_icon_cache_update
}
