# Copyright 2020-2022 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
VIRTUALX_REQUIRED="pgo"
WANT_AUTOCONF="2.1"
MOZ_ESR=""

# This list can be updated with scripts/get_langs.sh from the mozilla overlay
MOZ_LANGS=( ach af an ar as ast az bg bn-BD bn-IN br bs ca cak cs cy da de dsb
el en en-GB en-US en-ZA eo es-AR es-CL es-ES es-MX et eu fa ff fi fr fy-NL ga-IE
gd gl gn gu-IN he hi-IN hr hsb hu hy-AM id is it ja ka kab kk km kn ko lij lt lv
mai mk ml mr ms nb-NO nl nn-NO or pa-IN pl pt-BR pt-PT rm ro ru si sk sl son sq
sr sv-SE ta te th tr uk uz vi xh zh-CN zh-TW )

inherit autotools check-reqs desktop flag-o-matic multilib pax-utils toolchain-funcs xdg-utils

DESCRIPTION="Waterfox Web Browser Classic Version"
HOMEPAGE="https://www.waterfox.net/"
KEYWORDS="~amd64 ~x86"
SLOT="0"
LICENSE="MPL-2.0 GPL-2 LGPL-2.1"
IUSE="ccache eme-free +gmp-autoupdate hardened hwaccel jack nsplugin pgo test dbus debug neon pulseaudio selinux startup-notification system-jpeg system-libevent system-sqlite system-libvpx force-gtk3 gtk2 wifi custom-cflags custom-optimization"
SRC_URI="
	https://github.com/WaterfoxCo/Waterfox-Classic/archive/refs/tags/${PV}-classic.tar.gz -> ${P}.tar.gz
	"

ASM_DEPEND=">=dev-lang/yasm-1.1"

RDEPEND="
	wifi? (
		kernel_linux? ( >=sys-apps/dbus-0.60
						>=dev-libs/dbus-glib-0.72
						net-misc/networkmanager )
	)
	force-gtk3? ( >=x11-libs/gtk+-3.4.0:3 )
	!gtk2? ( >=x11-libs/gtk+-3.4.0:3 )
	jack? ( virtual/jack )
	>=app-text/hunspell-1.5.4:=
	dev-libs/atk
	dev-libs/expat
	>=x11-libs/cairo-1.10[X]
	>=x11-libs/gtk+-2.18:2
	x11-libs/gdk-pixbuf
	>=x11-libs/pango-1.22.0
	>=media-libs/libpng-1.6.31:0=[apng]
	>=media-libs/mesa-10.2:*
	>=media-libs/fontconfig-2.7.0
	>=media-libs/freetype-2.4.10
	kernel_linux? ( !pulseaudio? ( media-libs/alsa-lib ) )
	pulseaudio? ( || ( media-sound/pulseaudio
				>=media-sound/apulse-0.1.9 ) )
	virtual/freedesktop-icon-theme
	dbus? ( >=sys-apps/dbus-0.60
			>=dev-libs/dbus-glib-0.72 )
	startup-notification? ( >=x11-libs/startup-notification-0.8 )
	>=x11-libs/pixman-0.19.2
	>=dev-libs/glib-2.26:2
	>=sys-libs/zlib-1.2.3
	>=dev-libs/libffi-3.0.10
	media-video/ffmpeg[x264]
	x11-libs/libX11
	x11-libs/libXcomposite
	x11-libs/libXdamage
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXrender
	x11-libs/libXt
	system-jpeg? ( >=media-libs/libjpeg-turbo-1.2.1 )
	system-libevent? ( >=dev-libs/libevent-2.0:0= )
	system-sqlite? ( >=dev-db/sqlite-3.33.0:3[secure-delete,debug=] )
	system-libvpx? ( >=media-libs/libvpx-1.5.0:0=[postproc] )
	selinux? ( sec-policy/selinux-mozilla )"

DEPEND="${RDEPEND}
	pgo? ( >=sys-devel/gcc-4.5 )
	virtual/rust
	amd64? ( ${ASM_DEPEND} virtual/opengl )
	x86? ( ${ASM_DEPEND} virtual/opengl )
	app-arch/zip
	app-arch/unzip[-natspec]
	>=sys-devel/binutils-2.16.1
	sys-apps/findutils
	virtual/pkgconfig"

# Python 2.7 still used for mach build system
#	${PYTHON_DEPS}
BDEPEND="${RDEPEND}
	dev-lang/python:2.7
	ccache? ( || (	dev-util/ccache
					dev-util/sccache ) )
"

S="${WORKDIR}/Waterfox-Classic-${PV}-classic"
BUILD_OBJ_DIR="${S}/wf"

# allow GMP_PLUGIN_LIST to be set in an eclass or
# overridden in the enviromnent (advanced hackers only)
if [[ -z ${GMP_PLUGIN_LIST} ]]; then
	GMP_PLUGIN_LIST=( gmp-gmpopenh264 gmp-widevinecdm )
fi

pkg_setup() {
	# Ensure we use C locale when building
	export LANG="C"
	export LC_ALL="C"
	export LC_MESSAGES="C"
	export LC_CTYPE="C"

	# Ensure we use correct toolchain
	export HOST_CC="$(tc-getBUILD_CC)"
	export HOST_CXX="$(tc-getBUILD_CXX)"
	tc-export CC CXX LD PKG_CONFIG

	# Ensure that we have a sane build enviroment
	export MOZILLA_CLIENT=1
	export BUILD_OPT=1
	export NO_STATIC_LIB=1
	export USE_PTHREADS=1
	export ALDFLAGS=${LDFLAGS}
	# ensure MOZCONFIG is not defined
	unset MOZCONFIG

	# set MOZILLA_FIVE_HOME
	export MOZILLA_FIVE_HOME="/usr/$(get_libdir)/${PN}"

	# nested configure scripts in mozilla products generate unrecognized options
	# false positives when toplevel configure passes downwards.
	export QA_CONFIGURE_OPTIONS=".*"

	# Avoid PGO profiling problems due to enviroment leakage
	# These should *always* be cleaned up anyway
	unset DBUS_SESSION_BUS_ADDRESS \
		DISPLAY \
		ORBIT_SOCKETDIR \
		SESSION_MANAGER \
		XDG_SESSION_COOKIE \
		XAUTHORITY

	if use pgo; then
		einfo
		ewarn "You will do a double build for profile guided optimization."
		ewarn "This will result in your build taking at least twice as long as before."
	fi
}

pkg_pretend() {
	# Ensure we have enough disk space to compile
	if use pgo || use debug || use test ; then
		CHECKREQS_DISK_BUILD="8G"
	else
		CHECKREQS_DISK_BUILD="4G"
	fi
	check-reqs_pkg_setup
}

src_prepare() {
#	local patch_exclude=(
#		1000_gentoo_install_dir.patch
#		2001_system_harfbuzz.patch
#		2002_system_graphite2.patch
#	)

#	local mpatch
#	for mpatch in ${patch_exclude[@]} ; do
#		if [[ -f "${WORKDIR}"/waterfox/${mpatch} ]] ; then
#			rm "${WORKDIR}"/waterfox/${mpatch} || die
#		fi
#	done

	# Apply our patches
	eapply ${FILESDIR}/waterfox/*
	eapply "${FILESDIR}/c++14.patch"
	use system-libvpx && eapply -p2 "${FILESDIR}/seamonkey-2.53.3-system_libvpx-1.8.patch"

	# Enable gnomebreakpad
	if use debug ; then
		sed -i -e "s:GNOME_DISABLE_CRASH_DIALOG=1:GNOME_DISABLE_CRASH_DIALOG=0:g" \
			"${S}"/build/unix/run-mozilla.sh || die "sed failed!"
	fi

	# Drop -Wl,--as-needed related manipulation for ia64 as it causes ld sefgaults, bug #582432
	if use ia64 ; then
		sed -i \
		-e '/^OS_LIBS += no_as_needed/d' \
		-e '/^OS_LIBS += as_needed/d' \
		"${S}"/widget/gtk/mozgtk/gtk2/moz.build \
		"${S}"/widget/gtk/mozgtk/gtk3/moz.build \
		|| die "sed failed to drop --as-needed for ia64"
	fi

	# Ensure that our plugins dir is enabled as default
	sed -i -e "s:/usr/lib/mozilla/plugins:/usr/lib/nsbrowser/plugins:" \
		"${S}"/xpcom/io/nsAppFileLocationProvider.cpp || die "sed failed to replace plugin path for 32bit!"
	sed -i -e "s:/usr/lib64/mozilla/plugins:/usr/lib64/nsbrowser/plugins:" \
		"${S}"/xpcom/io/nsAppFileLocationProvider.cpp || die "sed failed to replace plugin path for 64bit!"

	# Fix sandbox violations during make clean, bug 372817
	sed -e "s:\(/no-such-file\):${T}\1:g" \
		-i "${S}"/config/rules.mk \
		-i "${S}"/nsprpub/configure{.in,} \
		|| die

	# Don't exit with error when some libs are missing which we have in
	# system.
	sed '/^MOZ_PKG_FATAL_WARNINGS/s@= 1@= 0@' -i "${S}"/browser/installer/Makefile.in || die

	# Don't error out when there's no files to be removed:
	sed 's@\(xargs rm\)$@\1 -f@' -i "${S}"/toolkit/mozapps/installer/packager.mk || die

	# Allow user to apply any additional patches without modifing ebuild
	eapply_user

	# Autotools configure is now called old-configure.in
	# This works because there is still a configure.in that happens to be for the
	# shell wrapper configure script
	eautoreconf old-configure.in

	# Must run autoconf in js/src
	cd "${S}"/js/src || die
	eautoconf old-configure.in
}

src_configure() {
	MEXTENSIONS="default"
	# Google API keys (see http://www.chromium.org/developers/how-tos/api-keys)
	# Note: These are for Gentoo Linux use ONLY. For your own distribution, please
	# get your own set of keys.
	_google_api_key=AIzaSyDEAOvatFo0eTgsV_ZlEzx0ObmepsMzfAc

	####################################
	# mozconfig, CFLAGS and CXXFLAGS setup
	####################################
	rm ${S}/.mozconfig
	declare enable_optimize pango_version myext x
	declare WF=$([[ ${PN} == waterfox ]] && echo true || echo false)

	####################################
	# Setup the initial .mozconfig
	# See http://www.mozilla.org/build/configure-build.html
	####################################
	cp browser/config/mozconfig .mozconfig || die "cp browser/config/mozconfig failed"

	####################################
	# CFLAGS setup and ARCH support
	####################################
	# Set optimization level
	if [[ $(gcc-major-version) -ge 7 ]]; then
		echo "ac_add_options --enable-optimize=-O2 # Workaround known breakage" >>.mozconfig
	elif [[ ${ARCH} == hppa ]]; then
		echo "ac_add_options --enable-optimize=-O0 # more than -O0 causes a segfault on hppa" >>.mozconfig
	elif [[ ${ARCH} == x86 ]]; then
		echo "ac_add_options --enable-optimize=-O2 # less then -O2 causes a segfault on x86" >>.mozconfig
	elif use custom-optimization || [[ ${ARCH} =~ (alpha|ia64) ]]; then
		# Set optimization level based on CFLAGS
		if is-flag -O0; then
			echo "ac_add_options --enable-optimize=-O0 # from CFLAGS" >>.mozconfig
		elif [[ ${ARCH} == ppc ]] && has_version '>=sys-libs/glibc-2.8'; then
			echo "ac_add_options --enable-optimize=-O1 # more than -O1 segfaults on ppc with glibc-2.8" >>.mozconfig
		elif is-flag -O4; then
			echo "ac_add_options --enable-optimize=-O4 # from CFLAGS" >>.mozconfig
		elif is-flag -O3; then
			echo "ac_add_options --enable-optimize=-O3 # from CFLAGS" >>.mozconfig
		elif is-flag -O1; then
			echo "ac_add_options --enable-optimize=-O1 # from CFLAGS" >>.mozconfig
		elif is-flag -Os; then
			echo "ac_add_options --enable-optimize=-Os # from CFLAGS" >>.mozconfig
		else
			echo "ac_add_options --enable-optimize=-O2 # Gentoo's default optimization" >>.mozconfig
		fi
	else
		# Enable Mozilla's default
		echo "ac_add_options --enable-optimize # mozilla default" >>.mozconfig
	fi

	# Strip optimization so it does not end up in compile string
	filter-flags '-O*'

	# Strip over-aggressive CFLAGS
	use custom-cflags || strip-flags

	# Additional ARCH support
	case "${ARCH}" in
		arm)
			# Reduce the memory requirements for linking
			append-ldflags -Wl,--no-keep-memory -Wl,--reduce-memory-overheads
			;;
		alpha)
			# Historically we have needed to add -fPIC manually for 64-bit.
			# Additionally, alpha should *always* build with -mieee for correct math
			# operation
			append-flags -fPIC -mieee
			;;
		ia64)
			# Historically we have needed to add this manually for 64-bit
			append-flags -fPIC
			;;
		ppc64)
			append-flags -fPIC -mminimal-toc
			# Reduce the memory requirements for linking
			append-ldflags -Wl,--no-keep-memory -Wl,--reduce-memory-overheads
			;;
	esac

	# We need to append flags for gcc-6 support
	if [[ $(gcc-major-version) -ge 6 ]]; then
		append-cxxflags -fno-delete-null-pointer-checks -fno-lifetime-dse -fno-schedule-insns2
	fi

	# Use the MOZILLA_FIVE_HOME for the rpath
	append-ldflags -Wl,-rpath="${MOZILLA_FIVE_HOME}",--enable-new-dtags
	# Set MOZILLA_FIVE_HOME in mozconfig
	echo "ac_add_options  --with-default-mozilla-five-home=${MOZILLA_FIVE_HOME} # " >>.mozconfig

	####################################
	# mozconfig setup
	####################################
	echo "ac_add_options --disable-updater # disable_update_strip" >>.mozconfig
	echo "ac_add_options --disable-strip # disable_update_strip" >>.mozconfig
	echo "ac_add_options --disable-install-strip # disable_update_strip" >>.mozconfig

	# jemalloc won't build with older glibc
	! has_version ">=sys-libs/glibc-2.4" && echo "ac_add_options --disable-jemalloc # we have old glibc" >>.mozconfig

	echo "ac_add_options --with-system-zlib # system_libs" >>.mozconfig
	echo "ac_add_options --with-system-bz2 # system_libs" >>.mozconfig

	# Must pass release in order to properly select linker via gold useflag
	echo "ac_add_options --enable-release # Enable by Gentoo" >>.mozconfig

	# Must pass --enable-gold if using ld.gold
	if tc-ld-is-gold ; then
		echo "ac_add_options --enable-gold # tc-ld-is-gold=true" >>.mozconfig
	else
		echo "ac_add_options --disable-gold # tc-ld-is-gold=false" >>.mozconfig
	fi

	if has bindist ${IUSE}; then
		echo "ac_add_options --enable-official-branding # !+bindist" >>.mozconfig
		if [[ ${PN} == firefox ]] && use bindist ; then
			echo "ac_add_options --with-branding=browser/branding/aurora # " >>.mozconfig
		fi
	fi

	# Enable position independent executables
	echo "ac_add_options --enable-pie # enabled by Gentoo" >>.mozconfig
	echo "ac_add_options --disable-tests # debug" >>.mozconfig

	if ! use debug ; then
		echo "ac_add_options --disable-debug # " >>.mozconfig
		echo "ac_add_options --disable-debug-symbols # disabled by Gentoo" >>.mozconfig
	else
		echo "ac_add_options --enable-debug-symbols # enabled by Gentoo" >>.mozconfig
		echo "ac_add_options --enable-debug # " >>.mozconfig
	fi

	if ! use ccache; then
		echo "ac_add_options --disable-ccache # disable ccache" >>"${S}"/.mozconfig
	else
		echo "ac_add_options --with-ccache # enable ccache" >>"${S}"/.mozconfig
	fi

	if use startup-notification; then
		echo "ac_add_options --enable-startup-notification # " >>.mozconfig
	else
		echo "ac_add_options --disable-startup-notification # " >>.mozconfig
	fi

	if use wifi; then
		# wifi pulls in dbus so manage both here
		echo "ac_add_options --enable-necko-wifi # wifi" >>.mozconfig
		if use kernel_linux && use wifi && ! use dbus; then
			echo "Enabling dbus support due to wifi request"
			echo "ac_add_options --enable-dbus # dbus required by necko-wifi on linux" >>.mozconfig
		else
			echo "ac_add_options --enable-dbus # " >>.mozconfig
		fi
	else
		if use dbus; then
			echo "ac_add_options --enable-dbus # " >>.mozconfig
		else
			echo "ac_add_options --disable-dbus # " >>.mozconfig
		fi
		echo "ac_add_options --disable-necko-wifi # disabled" >>.mozconfig
	fi

	if [[ -n ${MOZCONFIG_OPTIONAL_JIT} ]]; then
		echo "ac_add_options --enable-ion # jit" >>.mozconfig
	fi

	# These are enabled by default in all mozilla applications
	echo "ac_add_options --x-includes="${SYSROOT}${EPREFIX}"/usr/include  # " >>.mozconfig
	echo "ac_add_options --x-libraries="${SYSROOT}${EPREFIX}"/usr/$(get_libdir) # " >>.mozconfig

	if use system-libevent; then
		echo "ac_add_options --with-system-libevent="${SYSROOT}${EPREFIX}"/usr # " >>.mozconfig
	fi

	echo "ac_add_options --prefix="${EPREFIX}"/usr # " >>.mozconfig
	echo "ac_add_options --libdir="${EPREFIX}"/usr/$(get_libdir) # " >>.mozconfig
	echo "ac_add_options --enable-system-hunspell # Gentoo default" >>.mozconfig
	echo "ac_add_options --disable-crashreporter # " >>.mozconfig
	echo "ac_add_options --with-system-png # Gentoo default " >>.mozconfig
	echo "ac_add_options --enable-system-ffi # " >>.mozconfig
	echo "ac_add_options --disable-gconf # " >>.mozconfig
	echo "ac_add_options --with-intl-api # " >>.mozconfig
	echo "ac_add_options --enable-skia # " >>.mozconfig

	# default toolkit is cairo-gtk3, optional use flags can change this
	local toolkit="cairo-gtk3"
	local toolkit_comment=""
	if [[ -n ${MOZCONFIG_OPTIONAL_GTK3} ]]; then
		if ! use force-gtk3; then
			toolkit="cairo-gtk2"
			toolkit_comment="force-gtk3 use flag"
		fi
	fi
	if [[ -n ${MOZCONFIG_OPTIONAL_GTK2ONLY} ]]; then
		if use gtk2 ; then
			toolkit="cairo-gtk2"
		else
			toolkit_comment="gtk2 use flag"
		fi
	fi

	echo "ac_add_options --enable-default-toolkit=${toolkit} # ${toolkit_comment} " >>.mozconfig

	# Instead of the standard --build= and --host=, mozilla uses --host instead
	# of --build, and --target intstead of --host.
	# Note, mozilla also has --build but it does not do what you think it does.
	# Set both --target and --host as mozilla uses python to guess values otherwise
	echo "ac_add_options --target="${CHOST}" # " >>.mozconfig
	echo "ac_add_options --host="${CBUILD:-${CHOST}}" # " >>.mozconfig

	# force the deprecated alsa sound code if pulseaudio is disabled
	if use kernel_linux && ! use pulseaudio ; then
		echo "ac_add_options --disable-pulseaudio # " >>.mozconfig
		echo "ac_add_options --enable-alsa # -pulseaudio" >>.mozconfig
	else
		echo "ac_add_options --enable-pulseaudio # " >>.mozconfig
	fi

	# For testing purpose only
	echo "ac_add_options --enable-content-sandbox # Sandbox" >>.mozconfig

	if use system-sqlite; then
		echo "ac_add_options --with-system-sqlite # +system-sqlite" >> "${S}/.mozconfig"
	else
		echo "ac_add_options --without-system-sqlite # -system-sqlite" >> "${S}/.mozconfig"
	fi

	if use system-jpeg; then
		echo "ac_add_options --with-system-jpeg # +system-jpeg" >> "${S}/.mozconfig"
	else
		echo "ac_add_options --without-system-jpeg # -system-jpeg" >> "${S}/.mozconfig"
	fi

	echo "ac_add_options --without-system-icu # -system-icu" >> "${S}/.mozconfig"

	if use system-libvpx; then
		echo "ac_add_options --with-system-libvpx # +system-libvpx" >> "${S}/.mozconfig"
	else
		echo "ac_add_options --without-system-libvpx # -system-libvpx" >> "${S}/.mozconfig"
	fi

	# Modifications to better support ARM, bug 553364
	if use neon ; then
		echo "ac_add_options --with-fpu=neon # " >>.mozconfig
		echo "ac_add_options --with-thumb=yes # " >>.mozconfig
		echo "ac_add_options --with-thumb-interwork=no # " >>.mozconfig
	fi
	if [[ ${CHOST} == armv* ]] ; then
		echo "ac_add_options --with-float-abi=hard # " >>.mozconfig
		if ! use system-libvpx ; then
			sed -i -e "s|softfp|hard|" \
			"${S}"/media/libvpx/moz.build
		fi
	fi

	if use jack; then
		echo "ac_add_options --enable-jack # +jack" >>.mozconfig
	else
		echo "ac_add_options --disable-jack # -jack" >>.mozconfig
	fi

	use eme-free && echo "ac_add_options --disable-eme # +eme-free" >>.mozconfig

	# It doesn't compile on alpha without this LDFLAGS
	use alpha && append-ldflags "-Wl,--no-relax"

	# Add full relro support for hardened
	if use hardened; then
		append-ldflags "-Wl,-z,relro,-z,now"
		echo "ac_add_options --enable-hardening # hardened" >>.mozconfig
	fi

	# Setup api key for location services
	echo -n "${_google_api_key}" > "${S}"/google-api-key
	echo "ac_add_options --with-google-api-keyfile="${S}/google-api-key" # " >>.mozconfig
	echo "ac_add_options  --enable-extensions="${MEXTENSIONS}" # " >>.mozconfig

	# Waterfox specific stuff
	echo "ac_add_options --disable-elf-hack # " >>.mozconfig
	echo "ac_add_options --enable-llvm-hacks # " >>.mozconfig
	echo "ac_add_options --with-app-name=${PN} # Waterfox " >>.mozconfig
	echo "ac_add_options --with-app-basename=${PN} # Waterfox" >>.mozconfig
	echo "ac_add_options --with-branding=browser/branding/unofficial # " >>.mozconfig
	echo "ac_add_options --with-distribution-id=org.waterfoxproject # Warerfox" >>.mozconfig
	echo "ac_add_options --disable-stylo # Waterfox" >>.mozconfig
	echo "ac_add_options --disable-gpsd # Waterfox" >>.mozconfig

	# Allow for a proper pgo build
	if use pgo; then
		echo "mk_add_options PROFILE_GEN_SCRIPT='EXTRA_TEST_ARGS=10 \$(MAKE) -C \$(MOZ_OBJDIR) pgo-profile-run'" >> "${S}"/.mozconfig
	fi

	echo "mk_add_options MOZ_OBJDIR=${BUILD_OBJ_DIR}" >> "${S}"/.mozconfig
	echo "mk_add_options XARGS=/usr/bin/xargs" >> "${S}"/.mozconfig

	# Finalize and report settings
	declare ac opt hash reason

	# Apply EXTRA_ECONF entries to .mozconfig
	if [[ -n ${EXTRA_ECONF} ]]; then
		IFS=\! read -a ac <<<${EXTRA_ECONF// --/\!}
		for opt in "${ac[@]}"; do
			echo "ac_add_options --${opt#--} # EXTRA_ECONF" >>.mozconfig
		done
	fi

	echo
	echo "=========================================================="
	echo "Building ${PF} with the following configuration"
	grep ^ac_add_options .mozconfig | while read ac opt hash reason; do
		[[ -z ${hash} || ${hash} == \# ]] \
		|| die "error reading mozconfig: ${ac} ${opt} ${hash} ${reason}"
		printf "    %-30s  %s\n" "${opt}" "${reason:-mozilla.org default}"
	done
	echo "=========================================================="
	echo

	# Force python 2.7 as not all script work with python 3 yet
	export PYTHON="python2.7"
	${S}/mach configure || die
}

src_compile() {
	MOZ_MAKE_FLAGS="${MAKEOPTS}" SHELL="${SHELL:-${EPREFIX}/bin/bash}" \
	${S}/mach build || die
}

src_install() {
	cd "${BUILD_OBJ_DIR}" || die

	# Pax mark xpcshell for hardened support, only used for startupcache creation.
	pax-mark m "${BUILD_OBJ_DIR}"/dist/bin/xpcshell

	# Add our default prefs for firefox
	cp "${FILESDIR}"/gentoo-default-prefs.js-1 \
		"${BUILD_OBJ_DIR}/dist/bin/browser/defaults/preferences/all-gentoo.js" || die

	local prefs_file="${BUILD_OBJ_DIR}/dist/bin/browser/defaults/preferences/all-gentoo.js"

	einfo "Adding prefs from mozconfig to ${prefs_file}"

	# set dictionary path, to use system hunspell
	echo "pref(\"spellchecker.dictionary_path\", \"${EPREFIX}/usr/share/myspell\");" \
			>>"${prefs_file}" || die

	# force cairo as the canvas renderer on platforms without skia support
	if [[ $(tc-endian) == "big" ]] ; then
		echo "sticky_pref(\"gfx.canvas.azure.backends\",\"cairo\");" >>"${prefs_file}" || die
		echo "sticky_pref(\"gfx.content.azure.backends\",\"cairo\");" >>"${prefs_file}" || die
	fi

	# Augment this with hwaccel prefs
	if use hwaccel ; then
		cat "${FILESDIR}"/gentoo-hwaccel-prefs.js-1 >> \
		"${BUILD_OBJ_DIR}/dist/bin/browser/defaults/preferences/all-gentoo.js" || die
	fi

	echo "pref(\"extensions.autoDisableScopes\", 3);" >> \
		"${BUILD_OBJ_DIR}/dist/bin/browser/defaults/preferences/all-gentoo.js" || die

	if use nsplugin; then
		echo "pref(\"plugin.load_flash_only\", false);" >> \
			"${BUILD_OBJ_DIR}/dist/bin/browser/defaults/preferences/all-gentoo.js" || die
	fi

	local plugin
	use gmp-autoupdate || use eme-free || for plugin in "${GMP_PLUGIN_LIST[@]}" ; do
		echo "pref(\"media.${plugin}.autoupdate\", false);" >> \
			"${BUILD_OBJ_DIR}/dist/bin/browser/defaults/preferences/all-gentoo.js" || die
	done

	MOZ_MAKE_FLAGS="${MAKEOPTS}" SHELL="${SHELL:-${EPREFIX}/bin/bash}" \
	emake DESTDIR="${D}" install

	local size sizes icon_path icon name
		sizes="16 22 24 32 256"
		icon_path="${S}/browser/branding/unofficial"
		icon="waterfox"
		name="Waterfox"

	# Install icons and .desktop for menu entry
	for size in ${sizes}; do
		insinto "/usr/share/icons/hicolor/${size}x${size}/apps"
		newins "${icon_path}/default${size}.png" "${icon}.png"
	done
	# The 128x128 icon has a different name
	insinto "/usr/share/icons/hicolor/128x128/apps"
	doins "${icon_path}/${icon}.png"
	# Install a 48x48 icon into /usr/share/pixmaps for legacy DEs
	newicon "${icon_path}/default48.png" "${icon}.png"
	newmenu "${FILESDIR}/icon/${PN}.desktop" "${PN}.desktop"
	sed -i -e "s:@NAME@:${name}:" -e "s:@ICON@:${icon}:" \
		"${ED%/}/usr/share/applications/${PN}.desktop" || die

	# Add StartupNotify=true bug 237317
	if use startup-notification ; then
		echo "StartupNotify=true"\
			 >> "${ED%/}/usr/share/applications/${PN}.desktop" || die
	fi

	# Required in order to use plugins and even run firefox on hardened.
	pax-mark m "${ED}"${MOZILLA_FIVE_HOME}/{waterfox,waterfox-bin,plugin-container}
}

pkg_preinst() {
	# if the apulse libs are available in MOZILLA_FIVE_HOME then apulse
	# doesn't need to be forced into the LD_LIBRARY_PATH
	if use pulseaudio && has_version ">=media-sound/apulse-0.1.9" ; then
		einfo "APULSE found - Generating library symlinks for sound support"
		local lib
		pushd "${ED}"${MOZILLA_FIVE_HOME} &>/dev/null || die
		for lib in ../apulse/libpulse{.so{,.0},-simple.so{,.0}} ; do
			# a quickpkg rolled by hand will grab symlinks as part of the package,
			# so we need to avoid creating them if they already exist.
			if ! [ -L ${lib##*/} ]; then
				ln -s "${lib}" ${lib##*/} || die
			fi
		done
		popd &>/dev/null || die
	fi
}

pkg_postinst() {
	# Update mimedb for the new .desktop file
	xdg_desktop_database_update

	if ! use gmp-autoupdate && ! use eme-free ; then
		elog "USE='-gmp-autoupdate' has disabled the following plugins from updating or"
		elog "installing into new profiles:"
		local plugin
		for plugin in "${GMP_PLUGIN_LIST[@]}"; do elog "\t ${plugin}" ; done
	fi

	if use pulseaudio && has_version ">=media-sound/apulse-0.1.9"; then
		elog "Apulse was detected at merge time on this system and so it will always be"
		elog "used for sound.  If you wish to use pulseaudio instead please unmerge"
		elog "media-sound/apulse."
	fi
}

pkg_postrm() {
	xdg_icon_cache_update
}
