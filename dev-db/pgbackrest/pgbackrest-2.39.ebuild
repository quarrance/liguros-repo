# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7
POSTGRES_COMPAT=( 13 14 )

inherit postgres

DESCRIPTION="Reliable PostgreSQL Backup & Restore"
HOMEPAGE="https://github.com/pgbackrest/pgbackrest"
SRC_URI="${HOMEPAGE}/archive/refs/tags/release/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	>=dev-db/postgresql-9.4
	dev-libs/libyaml
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}-release-${PV}/src"
