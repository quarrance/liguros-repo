# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit meson

DESCRIPTION="Lib that implements the client side of the SMTP protocol"
HOMEPAGE="https://libesmtp.github.io/"
SRC_URI="https://github.com/libesmtp/libESMTP/archive/v${PV/_}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/libESMTP-${PV}"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sparc ~x86 ~amd64-linux ~x86-linux"

LICENSE="LGPL-2.1 GPL-2"
SLOT="0/8"
IUSE="libressl ssl static-libs threads"

RDEPEND="
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
	)"
DEPEND="${RDEPEND}"

DOCS=( docs/{authors,bugreport,ChangeLog,faq,NEWS}.md README.md )

PATCHES=(
	"${FILESDIR}"/${P}-fix-soname.patch
)

src_configure() {
	local emesonargs=(
		-Ddefault_library="$(usex static-libs both shared)"
		$(meson_feature ssl tls)
		$(meson_feature threads pthreads)
	)
	meson_src_configure
}
