# Copyright 2020-2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7
#CROS_WORKON_PROJECT="platform/portage-derive"
#CROS_WORKON_COMMIT="a5bd54ca038a0be949adb812c8e952853d23c1b0" # v1.1.0

PYTHON_COMPAT=( python3_{7,8,9,10} )

#inherit distutils-r1 cros-workon
inherit distutils-r1

DESCRIPTION="Tool to help keep a derived Portage tree up-to-date"
HOMEPAGE="https://github.com/clipos/src_platform_portage-derive"
SRC_URI="${HOMEPAGE}/archive/${P}.tar.gz"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE="ipython"
S="${WORKDIR}/src_platform_${PN}-${P}"

DEPEND="
	ipython? ( dev-python/ipython[${PYTHON_USEDEP}] )
	sys-apps/portage[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"

python_compile() {
	(cd python && distutils-r1_python_compile)
}

python_install() {
	(cd python && distutils-r1_python_install)
}
