# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="PKCS#11 helper library"
HOMEPAGE="https://github.com/OpenSC/pkcs11-helper"
SRC_URI="https://github.com/OpenSC/${PN}/archive/refs/tags/${P}.tar.gz"

LICENSE="|| ( BSD GPL-2 )"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 ~hppa ~ia64 ~m68k ~mips ppc ppc64 ~s390 ~sparc x86"
IUSE="bindist doc gnutls nss ssl static-libs"

RDEPEND="
	ssl? (
		!gnutls? ( >=dev-libs/openssl-0.9.7:0=[bindist=] )
		gnutls? ( >=net-libs/gnutls-1.4.4 )
	)
	nss? ( dev-libs/nss )"
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig
	doc? ( >=app-doc/doxygen-1.4.7 )"

S="${WORKDIR}/${PN}-${P}"

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	econf \
		--disable-crypto-engine-polarssl \
		--disable-crypto-engine-mbedtls \
		--disable-crypto-engine-cryptoapi \
		$(use_enable doc) \
		$(use_enable !gnutls openssl) \
		$(use_enable !gnutls crypto-engine-openssl) \
		$(use_enable gnutls crypto-engine-gnutls) \
		$(use_enable nss crypto-engine-nss) \
		$(use_enable static-libs static)
}

src_install() {
	default
	find "${D}" -name '*.la' -delete || die
}
