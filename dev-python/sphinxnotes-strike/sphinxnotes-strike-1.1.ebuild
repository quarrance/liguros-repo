# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1

DESCRIPTION="Sphinx extension for strikethrough text support"
HOMEPAGE="https://sphinx.silverrainz.me/strike/"
SRC_URI="https://files.pythonhosted.org/packages/24/49/3d96c38b1da730ce30a27b32994214ff0000eb3e31876986b80f23b502a5/sphinxnotes-strike-1.1.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

distutils_enable_sphinx docs
distutils_enable_tests setup.py

