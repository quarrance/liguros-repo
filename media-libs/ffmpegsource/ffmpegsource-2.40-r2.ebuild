# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit autotools eutils flag-o-matic vcs-snapshot

DESCRIPTION="A libav/ffmpeg based source library for easy frame accurate access"
HOMEPAGE="https://github.com/FFMS/ffms2"
#SRC_URI="https://github.com/FFMS/ffms2/archive/${PV}.tar.gz -> ${P}.tar.gz"
GIT_COMMIT="90975eccfe7e2913dc8a66783ef9c021653d50f5"
SRC_URI="https://github.com/FFMS/ffms2/archive/${GIT_COMMIT}.tar.gz -> ${P}-${GIT_COMMIT}.tar.gz"

LICENSE="MIT"
SLOT="0/4"
KEYWORDS="amd64 x86"

RDEPEND="
	sys-libs/zlib
	media-video/ffmpeg:=
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
"

S=${WORKDIR}/${P}-${GIT_COMMIT}

ffms_check_compiler() {
	if [[ ${MERGE_TYPE} != "binary" ]] && ! test-flag-CXX -std=c++11; then
		die "Your compiler lacks C++11 support. Use GCC>=4.7.0 or Clang>=3.3."
	fi
}

pkg_pretend() {
	ffms_check_compiler
}

pkg_setup() {
	ffms_check_compiler
}

src_prepare() {
	default_src_prepare
	${S}/autogen.sh
}

src_install() {
	default_src_install
	find "${D}" -name '*.la' -delete || die
}
