# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit webapp

DESCRIPTION="Web-based browsing tool for Subversion (SVN) repositories in PHP"
HOMEPAGE="https://websvnphp.github.io/"
SRC_URI="https://github.com/websvnphp/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
IUSE="enscript"
KEYWORDS="amd64 ppc ppc64 ~sparc x86"

DEPEND=""
RDEPEND="dev-lang/php:*[xml]
	dev-vcs/subversion
	virtual/httpd-php:*
	enscript? ( app-text/enscript )"
RESTRICT="mirror"

src_install() {
	webapp_src_preinst

	DOCS=( changes.txt )
	einstalldocs

	mv include/{dist,}config.php
	rm -rf license.txt changes.txt doc/

	insinto "${MY_HTDOCSDIR}"
	doins -r .

	webapp_configfile "${MY_HTDOCSDIR}"/include/config.php
	webapp_serverowned "${MY_HTDOCSDIR}"/cache
	webapp_src_install
}
