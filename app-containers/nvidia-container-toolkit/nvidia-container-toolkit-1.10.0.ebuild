# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

EGO_PN="github.com/NVIDIA/${PN}"

inherit go-module

DESCRIPTION="NVIDIA container runtime toolkit"
HOMEPAGE="https://github.com/NVIDIA/nvidia-container-toolkit"
SRC_URI="
	https://github.com/NVIDIA/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"
KEYWORDS="~amd64"
LICENSE="Apache-2.0"
SLOT="0"
IUSE=""

RDEPEND="
	sys-libs/libnvidia-container
"

DEPEND="${RDEPEND}"

BDEPEND="
	app-arch/unzip
	sys-devel/make
"

src_unpack() {
	default
}

src_compile() {
	emake binaries || die
}

src_install() {
	dobin "${PN}"
	into "/usr/bin"
	dosym "${PN}" "/usr/bin/nvidia-container-runtime-hook"
	insinto "/etc/nvidia-container-runtime"
	doins "${FILESDIR}/config.toml"
}

pkg_postinst() {
	elog "Your docker service must restart after install this package."
	elog "OpenRC: sudo rc-service docker restart"
	elog "systemd: sudo systemctl restart docker"
	elog "You may need to edit your /etc/nvidia-container-runtime/config.toml"
	elog "file before running ${PN} for the first time."
	elog "For details, please see the NVIDIA docker manual page."
}
