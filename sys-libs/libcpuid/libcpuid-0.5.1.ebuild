# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit autotools

DESCRIPTION="A small C library for x86 (and x86_64) CPU detection and feature extraction"
HOMEPAGE="https://github.com/anrieff/libcpuid"
SRC_URI="https://github.com/anrieff/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="BSD-2"
SLOT="0"
KEYWORDS="-* amd64 x86"
IUSE="test"
RESTRICT="primaryuri"

src_prepare() {
	default
	eautoreconf
}

src_test() {
	emake test
}
