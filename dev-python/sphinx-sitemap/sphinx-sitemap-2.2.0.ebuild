# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"
RESTRICT="test"

inherit distutils-r1

DESCRIPTION="Sitemap generator for Sphinx"
HOMEPAGE="https://github.com/jdillard/sphinx-sitemap"
SRC_URI="https://files.pythonhosted.org/packages/94/d4/408579f209dfc6c95e0b31e72e67cc94de78dfb8936b1009f68b4d56d7f5/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

distutils_enable_sphinx docs
distutils_enable_tests setup.py

