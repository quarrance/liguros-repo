# Copyright 2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

MODULE_AUTHOR=cbbrowne
MODULE_VERSION=1.9

DESCRIPTION="Create documentation from PostgreSQL database"

inherit git-r3

HOMEPAGE="https://github.com/cbbrowne/autodoc"
EGIT_REPO_URI="https://github.com/cbbrowne/autodoc"

SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	dev-perl/DBI
	dev-perl/HTML-Template
	dev-perl/TermReadKey
	dev-perl/DBD-Pg
"

DEPEND="${RDEPEND}"

src_configure() {
	sed -i -e 's/PREFIX = \/usr\/local/PREFIX = \/usr/' Makefile
}

