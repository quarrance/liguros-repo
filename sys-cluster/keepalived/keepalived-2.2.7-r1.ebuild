# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="A strong & robust keepalive facility to the Linux Virtual Server project"
HOMEPAGE="https://www.keepalived.org/"
SRC_URI="https://www.keepalived.org/software/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sparc ~x86"
IUSE="dbus debug ipv6 -json libressl snmp"

RDEPEND="dev-libs/libnl:=
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )
	dev-libs/popt
	net-libs/libnfnetlink
	sys-apps/iproute2
	dbus? ( sys-apps/dbus )
	json? ( dev-libs/json-c:= )
	snmp? ( net-analyzer/net-snmp )"
DEPEND="
	${RDEPEND}
	>=sys-kernel/linux-headers-4.4
"

DOCS=(
	README CONTRIBUTORS INSTALL ChangeLog AUTHOR TODO
	doc/keepalived.conf.SYNOPSIS doc/NOTE_vrrp_vmac.txt
)

src_prepare() {
	default
	eapply ${FILESDIR}/SSL_set0_wbio.patch

	eautoreconf
}

src_configure() {
	STRIP=/bin/true \
	econf \
		--with-kernel-dir=/usr \
		--enable-sha1 \
		--enable-vrrp \
		$(use_enable dbus) \
		$(use_enable dbus dbus-create-instance) \
		$(use_enable debug) \
		$(use_enable json) \
		$(use_enable snmp)
}

src_install() {
	default

	newinitd "${FILESDIR}"/${PN}.init ${PN}
	newconfd "${FILESDIR}"/${PN}.confd ${PN}

	use snmp && dodoc doc/KEEPALIVED-MIB.txt

	# This was badly named by upstream, it's more HOWTO than anything else.
	newdoc INSTALL INSTALL+HOWTO
	# Clean up sysvinit files
	rm -rv "${ED}"/etc/sysconfig || die
}
