# Copyright 2020-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

LUA_COMPAT=( lua5-{1,3,4} luajit )

inherit lua-single

DESCRIPTION="A Lua-based environment module system supporting TCL and software hierarchy"
HOMEPAGE="https://www.tacc.utexas.edu/tacc-projects/lmod"
SRC_URI="https://github.com/TACC/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-lua/luaposix
		dev-lang/tcl
		dev-lua/lua-bit32
		>=dev-lua/luafilesystem-1.6.2"
RDEPEND="${DEPEND}"

LDIR="/etc/Lmod"
MDIR="${LDIR}/modules"
SDIR="${LDIR}/cacheDir"

pkg_setup() {
	lua-single_pkg_setup
	# get first two lua version numbers
	LUASHORT=`echo $(lua_get_version) | cut -d'.' -f 1-2`
}

src_configure() {
	LUABIN="/usr/bin/lua${LUASHORT}"
	LUACBIN="/usr/bin/luac${LUASHORT}"
	${S}/configure --with-module-root-path=${MDIR} --with-spiderCacheDir=${SDIR} --with-updateSystemFn=${LDIR}/system.txt --with-lua="${LUABIN}" --with-luac="${LUACBIN}"
}

src_install() {
	dodir ${MDIR}
	dodir ${SDIR}
	doins -r "${FILESDIR}/etc"
	default
}

pkg_postinst() {
	elog "You should add the following to your ~/.bashrc to use Lmod:"
	elog "export MODULEPATH=/usr/lmod/lmod/modulefiles:${MDIR}"
	elog "[ -f /usr/lmod/lmod/init/bash ] && \\ "
	elog "    source /usr/lmod/lmod/init/bash"
	elog ""
	elog "You may also want to run the following commands to update the system cache dir:"
	elog "export MODULEPATH=/usr/lmod/lmod/modulefiles:${MDIR}"
	elog "update_lmod_system_cache_files -d ${SDIR} -t ${MDIR}/system.txt $MODULEPATH"
}
