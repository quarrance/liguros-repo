# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="Basic decoder for Apple Lossless Audio Codec files (ALAC)"
HOMEPAGE="https://web.archive.org/web/20150427073148/https://craz.net/programs/itunes/alac.html"
SRC_URI="https://web.archive.org/web/20150427073148/https://craz.net/programs/itunes/alac.html"
S="${WORKDIR}/${PN}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ppc x86 ~ppc-macos ~x86-solaris"

PATCHES=( "${FILESDIR}"/${PN}-0.2.0-fix-build-system.patch )

src_configure() {
	tc-export CC
}

src_install() {
	dobin alac
	einstalldocs
}
