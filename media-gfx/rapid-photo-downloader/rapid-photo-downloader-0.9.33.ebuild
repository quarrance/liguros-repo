# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
DISTUTILS_USE_SETUPTOOLS=rdepend

inherit distutils-r1 xdg

DESCRIPTION="Import your images efficiently and reliably"
HOMEPAGE="https://damonlynch.net/rapid/"
SRC_URI="https://launchpad.net/rapid/pyqt/${PV}/+download/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	dev-python/arrow[${PYTHON_USEDEP}]
	dev-python/Babel[${PYTHON_USEDEP}]
	dev-python/colour[${PYTHON_USEDEP}]
	dev-python/easygui[${PYTHON_USEDEP}]
	dev-python/gphoto2[${PYTHON_USEDEP}]
	dev-python/pymediainfo[${PYTHON_USEDEP}]
	dev-python/PyQt5[${PYTHON_USEDEP}]
	dev-python/psutil[${PYTHON_USEDEP}]
	dev-python/pygobject[${PYTHON_USEDEP}]
	dev-python/python-dateutil[${PYTHON_USEDEP}]
	dev-python/pyxdg[${PYTHON_USEDEP}]
	dev-python/pyzmq[${PYTHON_USEDEP}]
	dev-python/rawkit[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	dev-python/sortedcontainers[${PYTHON_USEDEP}]
	dev-python/tenacity[${PYTHON_USEDEP}]
	dev-qt/qtimageformats:5
	dev-qt/qtsvg:5
	dev-util/desktop-file-utils
	media-libs/exiftool
	media-libs/gexiv2
	media-libs/gstreamer
	sys-fs/udisks
	x11-libs/libnotify
	x11-themes/hicolor-icon-theme
	www-servers/tornado
"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	dev-util/intltool
"

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
