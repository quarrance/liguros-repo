# Copyright 2020-2022 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit meson distutils-r1 multilib-minimal flag-o-matic

DESCRIPTION="A Vulkan and OpenGL overlay for monitoring FPS, temperatures and more"
HOMEPAGE="https://github.com/flightlessmango/MangoHud"
KEYWORDS="*"
IMGUI_VERSION="1.81"
VULKAN_VERSION="1.2.158"
SPDLOG_VERSION="1.8.5"
MY_PV=$(ver_cut 1-3)
SRC_URI="
	${HOMEPAGE}/archive/refs/tags/v${MY_PV}.tar.gz -> ${P}.tar.gz
	https://github.com/ocornut/imgui/archive/v${IMGUI_VERSION}.tar.gz -> imgui-${IMGUI_VERSION}.tar.gz
	https://wrapdb.mesonbuild.com/v1/projects/imgui/${IMGUI_VERSION}/1/get_zip -> imgui-${IMGUI_VERSION}-1-wrap.zip
	https://github.com/KhronosGroup/Vulkan-Headers/archive/v${VULKAN_VERSION}.tar.gz
	https://wrapdb.mesonbuild.com/v1/projects/vulkan-headers/${VULKAN_VERSION}/1/get_zip -> vulkan-headers-${VULKAN_VERSION}-1-wrap.zip
	https://github.com/gabime/spdlog/archive/v${SPDLOG_VERSION}.tar.gz -> spdlog-${SPDLOG_VERSION}.tar.gz
	https://wrapdb.mesonbuild.com/v2/spdlog_${SPDLOG_VERSION}-1/get_patch -> spdlog-${SPDLOG_VERSION}-1-wrap.zip
"
S="${WORKDIR}/MangoHud-${MY_PV}"

LICENSE="MIT"
SLOT="0"
IUSE="glvnd xnvctrl"

DEPEND="
	sys-apps/dbus
	x11-libs/libX11
	x11-libs/libdrm
	dev-python/mako[${PYTHON_USEDEP}]
	dev-util/glslang
	>=dev-util/vulkan-headers-1.2
	media-libs/vulkan-loader[${MULTILIB_USEDEP}]
	glvnd? (
		media-libs/libglvnd[$MULTILIB_USEDEP]
	)
	xnvctrl? (
		x11-drivers/nvidia-drivers[${MULTILIB_USEDEP},static-libs]
	)
"

RDEPEND="${DEPEND}"

src_prepare() {
	default

	# Move unpacked sources to subprojects folder
	mv ${WORKDIR}/Vulkan-Headers-${VULKAN_VERSION} subprojects || die
	mv ${WORKDIR}/imgui-${IMGUI_VERSION} subprojects || die
	mv ${WORKDIR}/spdlog-${SPDLOG_VERSION} subprojects || die
}

multilib_src_configure() {
	local emesonargs=(
		-Dappend_libdir_mangohud=false
		-Duse_system_vulkan=enabled
		-Dinclude_doc=false
		-Dwith_xnvctrl=$(usex xnvctrl enabled disabled)
		--wrap-mode nodownload
	)
	meson_src_configure
}

multilib_src_compile() {
	meson_src_compile
}

multilib_src_install() {
	meson_src_install
}

multilib_src_install_all() {
	dodoc "${S}/bin/MangoHud.conf"

	einstalldocs
}

pkg_postinst() {
	if ! use xnvctrl; then
		einfo ""
		einfo "If mangohud can't get GPU load, or other GPU information,"
		einfo "and you have an older Nvidia device."
		einfo ""
		einfo "Try enabling the 'xnvctrl' useflag."
		einfo ""
	fi
}
