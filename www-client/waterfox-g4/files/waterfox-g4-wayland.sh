#!/bin/sh

#
# Run Waterfox G4 under Wayland
#
export MOZ_ENABLE_WAYLAND=1
exec @PREFIX@/bin/waterfox-g4 "$@"
