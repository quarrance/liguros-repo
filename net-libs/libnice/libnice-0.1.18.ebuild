# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7
inherit meson xdg

DESCRIPTION="An implementation of the Interactice Connectivity Establishment standard (ICE)"
HOMEPAGE="https://nice.freedesktop.org"
SRC_URI="https://nice.freedesktop.org/releases/${P}.tar.gz"

LICENSE="|| ( MPL-1.1 LGPL-2.1 )"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 ~hppa ~ia64 ppc ppc64 sparc x86 ~amd64-linux ~x86-linux ~ppc-macos"
IUSE="+gnutls gstreamer introspection openssl ssl +upnp"

RDEPEND="
	>=dev-libs/glib-2.48:2
	introspection? ( >=dev-libs/gobject-introspection-1.30.0:= )
	gstreamer? ( media-libs/gstreamer )
	ssl? (
		gnutls?  ( >=net-libs/gnutls-2.12.0 )
		openssl? ( dev-libs/openssl )
	)
	upnp? ( >=net-libs/gupnp-igd-0.2.4 )
"
DEPEND="${RDEPEND}
	dev-util/glib-utils
	>=dev-util/gtk-doc-am-1.10
	virtual/pkgconfig
"
REQUIRED_USE="gnutls? ( !openssl )"

src_configure() {
	local mesonargs=(
		$(meson_use upnp)
		$(meson_use gstreamer)
		$(meson_feature introspection)
	)
	meson_src_configure
}

src_install() {
	einstalldocs
	find "${ED}" -name '*.la' -delete || die
	meson_src_install
}

src_test() {
	emake -j1 check
}
