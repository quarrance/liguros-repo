# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit go-module systemd user

FELIX_COMMIT="7a38390c65b65b11fdd3dd2f311e4c06543aecce"

KEYWORDS="~amd64"
DESCRIPTION="Calico's per-host agent, responsible for programming routes and security policy"
EGO_PN="github.com/projectcalico/felix"
HOMEPAGE="https://github.com/projectcalico/felix"
SRC_URI="
	https://${EGO_PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-r1-deps.tar.xz
"

LICENSE="Apache-2.0"
SLOT="0"
IUSE="+bird"

RESTRICT="test"

DEPEND=">=dev-libs/protobuf-3
	dev-go/gogo-protobuf"

RDEPEND="net-firewall/ipset
	bird? ( net-misc/bird )"

src_compile() {
	protoc --gogofaster_out=plugins=grpc:. proto/*.proto || die
	go build -v -o bin/calico-felix -ldflags \
		"-X buildinfo.GitVersion=${PV} \
		-X buildinfo.BuildDate=$(date -u +'%FT%T%z') \
		-X buildinfo.GitRevision=${FELIX_COMMIT}"  "${S}/cmd/calico-felix" || die
}

src_install() {
	dobin "bin/calico-${PN}"
	dodoc README.md
	insinto /etc/logrotate.d
	doins debian/calico-felix.logrotate
	insinto /etc/felix
	doins etc/felix.cfg.example
	newinitd "${FILESDIR}"/felix.initd felix
	newconfd "${FILESDIR}"/felix.confd felix
}
