# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="a simple video player that is synchronized to jack transport."
HOMEPAGE="http://xjadeo.sourceforge.net/"
SRC_URI="https://sourceforge.net/projects/${PN}/files/${PN}/v${PV}/${P}.tar.gz/download -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="alsa imlib osc osd qt4 sdl tools xv"

RDEPEND="virtual/jack
	media-video/ffmpeg:=
	alsa? ( >=media-libs/alsa-lib-1.0.10 )
	imlib? ( >=media-libs/imlib2-1.3.0 )
	osc? ( media-libs/liblo )
	sdl? ( >=media-libs/libsdl-1.2.8 )"

DEPEND="${RDPEND}
	>=sys-libs/zlib-1.2.2
	x11-misc/xcb
	qt4? ( dev-qt/qt3support
		>=dev-qt/qttest-4:4
	)
	virtual/pkgconfig"

DOCS=( AUTHORS ChangeLog README NEWS )

src_configure() {
	local myeconfargs=(
		$(use_enable imlib imlib2)
		$(use_enable osc)
		$(use_enable osd ft)
		$(use_enable qt4 qtgui)
		$(use_enable sdl)
		$(use_enable tools contrib)
		$(use_enable xv)
		--disable-timescale
		--disable-portmidi
		--enable-silent-rules
	)

	econf "${myeconfargs[@]}"
}

src_install() {
	default

	if use tools; then
		newdoc contrib/cli-remote/README README.cli-remote
		dobin contrib/cli-remote/jadeo-rcli
		insinto "/usr/share/${PN}"
		doins "contrib/${PN}-example.mp4"
	fi
}
