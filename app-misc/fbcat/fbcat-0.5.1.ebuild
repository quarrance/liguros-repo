# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Framebuffer grabber"
HOMEPAGE="https://github.com/jwilk/fbcat"
SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="GPL-2"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}
	media-libs/netpbm
	sys-apps/kbd
"
BDEPEND="
	app-text/docbook-xsl-stylesheets
	dev-libs/libxslt
"

src_compile() {
	default
	emake -C doc
}

src_install() {
	emake install \
		DESTDIR="${D}" \
		PREFIX="${EPREFIX}/usr"
}
