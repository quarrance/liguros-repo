# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7
inherit apache-module

DESCRIPTION="Reverse proxy add forward module"
HOMEPAGE="https://github.com/gnif/mod_rpaf"
SRC_URI="${HOMEPAGE}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND=""
RDEPEND=""

APACHE2_MOD_CONF="10_${PN}"
APACHE2_MOD_DEFINE="RPAF"
DOCFILES="CHANGES README.md"

need_apache2_4

src_install() {
	einfo "Installing Apache conf file ..."
	insinto "${APACHE_MODULES_CONFDIR}"
	set -- ${APACHE2_MOD_CONF}
	newins "${FILESDIR}/${1}.conf" "$(basename ${2:-$1}).conf" \
		|| die "internal ebuild error: '${FILESDIR}/${1}.conf' not found"

	einfo "Installing Apache module ..."
	TMPDIR=`apxs2 -q LIBEXECDIR`
	install -d ${D}${TMPDIR}/
	apxs2 -i -S LIBEXECDIR=${D}${TMPDIR}/ -n mod_rpaf.so mod_rpaf.la

	einfo "Installing docs ..."
	dodoc ${DOCFILES}
}
