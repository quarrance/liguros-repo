diff -urN postfix-3.6.3.orig/src/tls/tls_server.c postfix-3.6.3/src/tls/tls_server.c
--- postfix-3.6.3.orig/src/tls/tls_server.c	2021-11-24 14:22:53.090180021 +0100
+++ postfix-3.6.3/src/tls/tls_server.c	2021-11-24 14:23:44.571181378 +0100
@@ -502,7 +502,9 @@
 	 * ticket decryption callback already (since 2.11) asks OpenSSL to
 	 * avoid issuing new tickets when the presented ticket is re-usable.
 	 */
+#ifndef LIBRESSL_VERSION_NUMBER
 	SSL_CTX_set_num_tickets(server_ctx, 1);
+#endif
     }
 #endif
     if (!ticketable)
