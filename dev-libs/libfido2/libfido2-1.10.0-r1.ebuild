# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake udev linux-info

DESCRIPTION="Provides library functionality for FIDO 2.0"
HOMEPAGE="https://github.com/Yubico/libfido2"
SRC_URI="https://github.com/Yubico/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0/1"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sparc ~x86"
IUSE="libressl nfc +static-libs"

DEPEND="
	dev-libs/libcbor:=
	virtual/libudev:=
	app-text/mandoc
	sys-libs/zlib:0=
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )
"

RDEPEND="
	${DEPEND}
	acct-group/plugdev
"

PATCHES="
	$FILESDIR/libfido2-1.9.0-cmakelists.patch
	$FILESDIR/rs1.patch
	$FILESDIR/rs256.patch
"

pkg_pretend() {
	CONFIG_CHECK="
		~USB_HID
		~HIDRAW
	"

	check_extra_config
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_EXAMPLES=OFF
		-DBUILD_STATIC_LIBS=$(usex static-libs ON OFF)
		-DNFC_LINUX=$(usex nfc ON OFF)
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install
	udev_newrules udev/70-u2f.rules 70-libfido2-u2f.rules
}
