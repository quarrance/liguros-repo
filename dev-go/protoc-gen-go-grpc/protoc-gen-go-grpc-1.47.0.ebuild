# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module

DESCRIPTION="Generates Go language bindings of services in protobuf definition files for gRPC"
HOMEPAGE="https://pkg.go.dev/google.golang.org/grpc/cmd/protoc-gen-go-grpc"
SRC_URI="
	https://github.com/grpc/grpc-go/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"
LICENSE="BSD"
SLOT="0/${PVR}"
KEYWORDS="amd64 x86"
IUSE="test"
DEPEND="test? ( dev-libs/protobuf )"
RDEPEND=""
RESTRICT="!test? ( test )"

S="${WORKDIR}/grpc-go-${PV}/cmd/protoc-gen-go-grpc"

src_compile() {
	env GOBIN="${S}/bin" go install ./... ||
		die "compile failed"
}

src_install() {
	dobin bin/protoc-gen-go-grpc
}
