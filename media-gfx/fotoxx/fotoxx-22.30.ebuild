# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit desktop toolchain-funcs xdg-utils

DESCRIPTION="Program for improving image files made with a digital camera"
HOMEPAGE="https://www.kornelix.net/fotoxx/fotoxx.html"
SRC_URI="https://www.kornelix.net/downloads/downloads/${P}.tar.gz"
S="${WORKDIR}/${PN}"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~arm64 x86"
IUSE=""

DEPEND="
	media-libs/libpng
	media-libs/tiff
	media-libs/lcms:2
	x11-libs/gtk+:3"
RDEPEND="${DEPEND}
	media-gfx/dcraw
	media-libs/clutter-gtk
	media-libs/libchamplain[gtk]
	media-libs/libjpeg-turbo
	media-libs/exiftool
	x11-misc/xdg-utils"

src_compile() {
	tc-export CXX
	emake
}

src_install() {
	# For the Help menu items to work, *.html must be in /usr/share/doc/${PF},
	# and README, changelog, translations, edit-menus, KB-shortcuts must not be compressed
	emake DESTDIR="${D}" install
	rm -f "${D}"/usr/share/doc/${PF}/*.man || die
	docompress -x /usr/share/doc
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
