# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="tk"
DISTUTILS_USE_SETUPTOOLS=bdepend

inherit distutils-r1

DESCRIPTION="A module for very simple, very easy GUI programming in Python"
HOMEPAGE="https://github.com/robertlugg/easygui https://pypi.org/project/easygui/"
SRC_URI="https://files.pythonhosted.org/packages/91/d1/817a117333850b85d2a31767c8be8120a8f9492432e7b4c9c1c17699c979/easygui-0.98.2.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

src_prepare() {
	default
	touch ${S}/README.md
}
