# Copyright 2020-2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit cmake user systemd
DESCRIPTION="A lightweight continuous integration server"
HOMEPAGE="https://laminar.ohwg.net https://github.com/ohwgiles/laminar"
LICENSE="GPL-3"

SLOT="0"
KEYWORDS="~amd64 ~x86"
SRC_URI="https://github.com/ohwgiles/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
RESTRICT="network-sandbox"
IUSE=""
DEPEND=""
RDEPEND="
	${DEPEND}
	acct-group/laminar
	acct-user/laminar
	>=dev-libs/capnproto-0.7.0
	>=dev-libs/rapidjson-1.1.0
	>=dev-cpp/websocketpp-0.7.0
	>=dev-libs/boost-1.62
	dev-db/sqlite:3"

src_prepare() {
	eapply ${FILESDIR}/laminar-manpage.patch
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr"
		-DCMAKE_BUILD_TYPE=Release
	)

	cmake_src_configure
}

src_install() {
	newinitd "${FILESDIR}"/laminar.initd laminar
	newconfd "${FILESDIR}"/laminar.confd laminar
	doman ${S}/etc/laminarc.1
	doman ${S}/etc/laminard.8

	cmake_src_install
}

