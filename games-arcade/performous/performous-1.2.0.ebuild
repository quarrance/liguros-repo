# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

CMAKE_REMOVE_MODULES_LIST=( FindALSA FindBoost FindGettext FindJpeg FindPng FindTiff FindZ )
inherit cmake desktop xdg

DESCRIPTION="SingStar GPL clone"
HOMEPAGE="https://performous.org/"

SONGS_PN="ultrastar-songs"
SRC_URI="
	https://github.com/performous/performous/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	songs? (
		mirror://sourceforge/performous/${SONGS_PN}-restricted-3.zip
		mirror://sourceforge/performous/${SONGS_PN}-jc-1.zip
		mirror://sourceforge/performous/${SONGS_PN}-libre-3.zip
		mirror://sourceforge/performous/${SONGS_PN}-shearer-1.zip
	)
"

LICENSE="GPL-2
	songs? (
		CC-BY-NC-SA-2.5
		CC-BY-NC-ND-2.5
	)"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="midi songs tools webcam"
RESTRICT="network-sandbox"

RDEPEND="
	media-libs/aubio[fftw]
	dev-cpp/glibmm:2
	dev-cpp/libxmlpp:2.6
	dev-libs/boost[threads(+)]
	dev-libs/glib:2
	dev-libs/libxml2:2
	gnome-base/librsvg:2
	tools? ( media-gfx/imagemagick:0= )
	media-libs/libepoxy
	media-libs/libpng:0=
	media-libs/glm
	media-libs/libsdl2[joystick,video]
	media-libs/portaudio
	sys-libs/zlib
	virtual/ffmpeg
	virtual/glu
	virtual/jpeg:0
	virtual/libintl
	virtual/opengl
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/pango
	midi? ( media-libs/portmidi )
	webcam? ( media-libs/opencv )
"
DEPEND="
	${RDEPEND}
	media-libs/libepoxy
"
BDEPEND="
	sys-apps/help2man
	sys-devel/gettext
	songs? ( app-arch/unzip )
"

DOCS=( docs/{Authors,instruments}.txt )

PATCHES=(
	"${FILESDIR}"/${PN}-1.1-nomancompress.patch
	"${FILESDIR}"/ffmpeg.patch
)

src_unpack() {
	unpack ${P}.tar.gz

	use songs && unpack ${SONGS_PN}-restricted-3.zip
	use songs && unpack ${SONGS_PN}-jc-1.zip
	use songs && unpack ${SONGS_PN}-libre-3.zip
	use songs && unpack ${SONGS_PN}-shearer-1.zip
}

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr"
		-DENABLE_TOOLS="$(usex tools)"
		-DENABLE_WEBCAM="$(usex webcam)"
		-DENABLE_MIDI="$(usex midi)"
		-DENABLE_WEBSERVER=OFF
	)
	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_install() {
	cmake_src_install
	if use songs ; then
		insinto "${EPREFIX}/usr/share"/${PN}
		doins -r "${WORKDIR}/songs"
	fi

	newicon -s scalable data/themes/default/icon.svg ${PN}.svg
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
