# Copyright 2020-2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="Obarun library"
HOMEPAGE="https://framagit.org/Obarun/oblibs"
SRC_URI="https://framagit.org/Obarun/oblibs/-/archive/v${PV}/${PN}-v${PV}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	>=sys-devel/make-3.81
	>=dev-libs/skalibs-2.11.0.0
	>=dev-lang/execline-2.1.1.0
"

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
	econf \
		--dynlibdir=/usr/$(get_libdir) \
		--libdir=/usr/$(get_libdir)/${PN} \
		--with-dynlib=/usr/$(get_libdir) \
		--with-sysdeps=/usr/lib/skalibs
}

src_install() {
	emake DESTDIR="${D}" install
	dodoc AUTHORS
}
