# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="fetch, filter and deliver mail"
HOMEPAGE="https://github.com/nicm/fdm"
SRC_URI="https://github.com/nicm/fdm/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~x86"
IUSE="examples libressl pcre"

DEPEND="
	acct-group/fdm
	acct-user/fdm
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
	sys-libs/tdb
	pcre? ( dev-libs/libpcre )
"
RDEPEND="
	${DEPEND}
	acct-group/fdm
	acct-user/fdm
"

DOCS=( CHANGES README TODO MANUAL )

src_prepare() {
	default
	eautoreconf

	# Change user '_fdm' to 'fdm'
	sed -e 's/_fdm/fdm/g' -i fdm.h || die
}

src_configure() {
	econf $(use_enable pcre)
}

src_install() {
	default

	if use examples ; then
		docinto examples
		dodoc examples/*
	fi
}
