# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

WX_GTK_VER="3.0-gtk3"
LUA_COMPAT=( lua5-{1,3,4} luajit )

inherit wxwidgets xdg lua-single

DESCRIPTION="Reverse Engineers' Hex Editor"
HOMEPAGE="https://github.com/solemnwarning/rehex"
SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64"

RESTRICT="
	mirror
	!test? ( test )
"
LICENSE="GPL-2"
SLOT="0"
IUSE="test"

RDEPEND="${LUA_DEPS}
	dev-libs/capstone
	dev-libs/jansson
	dev-lua/busted
	x11-libs/wxGTK:${WX_GTK_VER}[X]
"
DEPEND="
	${RDEPEND}
	test? (
		dev-cpp/gtest
	)
"

src_configure() {
	export LUA_PKG=${ELUA}
	setup-wxwidgets
}

src_install() {
	emake LUA_PKG=${ELUA} prefix="${D}"/usr install
}
