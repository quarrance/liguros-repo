# Copyright 2020-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
KEYWORDS="~amd64"

inherit go-module bash-completion-r1

DESCRIPTION="The world's fastest framework for building websites"
HOMEPAGE="https://gohugo.io https://github.com/gohugoio/hugo"
SRC_URI="
	https://github.com/gohugoio/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="Apache-2.0 Unlicense BSD BSD-2 MPL-2.0"
SLOT="0"
IUSE="doc +sass"
KEYWORDS="~amd64 ~x86"
BDEPEND=">=dev-lang/go-1.18"
RDEPEND="
	media-libs/libwebp
	sass? ( dev-libs/libsass )
"
DEPEND="${RDEPEND}"

src_compile() {
	mkdir -pv bin || die
	go build -ldflags \
		"-X ${EGO_PN}/hugolib.CommitHash=${GIT_COMMIT}" \
		$(usex sass "-tags extended" "") -o "${S}/bin/hugo" || die
	bin/hugo gen man --dir man || die
	mkdir -pv completions || die
	bin/hugo completion bash > completions/hugo || die
	bin/hugo completion fish > completions/hugo.fish || die
	bin/hugo completion zsh > completions/_hugo || die

	if use doc ; then
		bin/hugo gen doc --dir doc || die
	fi
}

src_install() {
	dobin bin/*
	doman man/*
	dobashcomp completions/${PN}

	insinto /usr/share/fish/vendor_completions.d
	doins completions/${PN}.fish

	insinto /usr/share/zsh/site-functions
	doins completions/_${PN}

	if use doc ; then
		dodoc -r doc/
	fi
}
