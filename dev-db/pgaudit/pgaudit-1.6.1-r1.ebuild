# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7
POSTGRES_COMPAT=( 14 )

inherit postgres

MY_PV=${PV/_/-}

DESCRIPTION="PostgreSQL Audit Extension"
HOMEPAGE="https://www.pgaudit.org/"
SRC_URI="https://github.com/pgaudit/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="PostgreSQL"
SLOT="1.6"
KEYWORDS="~amd64"

DEPEND="
	dev-db/postgresql:14
"
RDEPEND="${DEPEND}"

DOCS=( CHANGELOG.md README.md RELEASENOTES.md )

S="${WORKDIR}/${PN}-${MY_PV}"

pkg_pretend() {
	postgres_check_slot
}

src_compile() {
	emake USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}

src_install() {
	emake DESTDIR="${D}" install USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}
