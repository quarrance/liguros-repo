# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"
RESTRICT="test"

inherit distutils-r1

DESCRIPTION="Sphinx extension for mocking directives and roles"
HOMEPAGE="https://sphinx.silverrainz.me/mock/"
SRC_URI="https://files.pythonhosted.org/packages/fb/b0/276ae8bfbadd11f573b708832f494bc288592cde7e5b455a472e7623d33d/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

distutils_enable_sphinx docs
distutils_enable_tests setup.py

