# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"
DISTUTILS_USE_SETUPTOOLS=pyproject.toml

inherit distutils-r1

DESCRIPTION="Zulip's PyPI packages"
HOMEPAGE="https://pypi.org/project/zulip/ https://github.com/zulip/python-zulip-api"
SRC_URI="https://github.com/zulip/python-${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE=""
DEPEND="
	>=dev-python/requests-0.12.1
	dev-python/distro
"
S="${WORKDIR}/python-${P}"

src_prepare() {
	EPYTHON=python3
	distutils-r1_src_prepare
}

src_compile() {
	cd ${S}/zulip
	default
	python setup.py build_ext --inplace
}

src_install() {
	cd ${S}/zulip
	default
	python setup.py install --root="${D}"
	python_optimize
}
