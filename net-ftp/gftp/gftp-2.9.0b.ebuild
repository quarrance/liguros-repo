# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
inherit gnome2

DESCRIPTION="Gnome based FTP Client"
HOMEPAGE="http://www.gftp.org"
SRC_URI="https://github.com/masneyb/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha amd64 ppc ppc64 sparc x86"
IUSE="gtk libressl ssl"

RDEPEND="
	dev-libs/glib:2
	sys-devel/gettext
	sys-libs/ncurses:0=
	sys-libs/readline:0
	gtk? ( x11-libs/gtk+:2 )
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= ) )
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
"

PATCHES=(
	# Fix SIGSEGV for gftp_expand_path function
	"${FILESDIR}/${PN}-2.7.0b-${PN}-expand-path-sigsegv.patch"
	"${FILESDIR}/${PN}-2.7.0b-desktop.patch"
)

src_prepare() {
	${S}/autogen.sh
	default
}

src_configure() {
	gnome2_src_configure \
		$(use_enable gtk gtkport) \
		$(use_enable ssl)
}

src_install() {
	gnome2_src_install
	dodoc docs/USERS-GUIDE
}
