# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
inherit distutils-r1

DESCRIPTION="Python bindings for PortAudio"
HOMEPAGE="https://github.com/spatialaudio/python-sounddevice/"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="virtual/python-cffi
	media-libs/portaudio"
RDEPEND="${DEPEND}"
