# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

CRATES="
aho-corasick-0.7.18
ansi_term-0.12.1
atty-0.2.14
autocfg-1.0.1
bitflags-1.3.2
block-buffer-0.7.3
block-padding-0.1.5
byte-tools-0.3.1
byteorder-1.4.3
cc-1.0.72
cfg-if-1.0.0
clap-3.0.13
digest-0.8.1
fake-simd-0.1.2
fblog-3.2.0
generic-array-0.12.4
handlebars-4.2.1
hashbrown-0.11.2
hermit-abi-0.1.19
hlua-0.4.1
indexmap-1.8.0
itoa-1.0.1
lazy_static-1.4.0
libc-0.2.116
log-0.4.14
lua52-sys-0.1.2
maplit-1.0.2
memchr-2.4.1
opaque-debug-0.2.3
os_str_bytes-6.0.0
pest-2.1.3
pest_derive-2.1.0
pest_generator-2.1.3
pest_meta-2.1.3
pkg-config-0.3.24
proc-macro2-1.0.36
quick-error-2.0.1
quote-1.0.15
regex-1.5.4
regex-syntax-0.6.25
ryu-1.0.9
serde-1.0.136
serde_json-1.0.78
sha-1-0.8.2
strsim-0.10.0
syn-1.0.86
termcolor-1.1.2
textwrap-0.14.2
typenum-1.15.0
ucd-trie-0.1.3
unicode-xid-0.2.2
winapi-0.3.9
winapi-i686-pc-windows-gnu-0.4.0
winapi-util-0.1.5
winapi-x86_64-pc-windows-gnu-0.4.0
"

inherit cargo

DESCRIPTION="Small command-line JSON Log viewer"
HOMEPAGE="https://github.com/brocode/fblog"
SRC_URI="$(cargo_crate_uris ${CRATES})
		${HOMEPAGE}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0 Boost-1.0 MIT Unlicense WTFPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc64"

DOCS=( README.org sample.json.log )

QA_FLAGS_IGNORED="/usr/bin/fblog"

src_install() {
	cargo_src_install
	einstalldocs
}
