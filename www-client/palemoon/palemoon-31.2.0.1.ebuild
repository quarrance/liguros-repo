# Copyright 2020-2022 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7
REQUIRED_BUILDSPACE='9G'

inherit eutils flag-o-matic pax-utils desktop xdg-utils

KEYWORDS="~x86 amd64"
DESCRIPTION="Pale Moon Web Browser"
HOMEPAGE="https://www.palemoon.org/"
SRC_URI="
	https://repo.palemoon.org/MoonchildProductions/Pale-Moon/archive/${PV}_Release.tar.gz -> ${P}.tar.gz
	https://repo.palemoon.org/MoonchildProductions/UXP/archive/RB_20220802.tar.gz
"

SLOT="0"
LICENSE="MPL-2.0 GPL-2 LGPL-2.1"
IUSE="+official-branding +optimize cpu_flags_x86_sse cpu_flags_x86_sse2 +shared-js threads debug +jemalloc -valgrind dbus -necko-wifi gnome +gconf +gtk2 -gtk3 pulseaudio +devtools -system-sqlite -system-cairo -system-pixman -system-libevent -system-hunspell -system-vpx -system-compress -system-images -system-nss -system-ffi"

RESTRICT="network-sandbox"

DEPEND="
	>=dev-lang/perl-5.6
	dev-lang/nasm
	sys-devel/gcc
	>=x11-libs/pango-1.22.0
	>=dev-libs/glib-2.22
	>=app-arch/brotli-1.0.9
	>=media-libs/woff2-1.0.2
"

RDEPEND="
	x11-libs/libXt
	media-libs/freetype
	>=media-libs/fontconfig-2.7.0
	optimize? ( sys-libs/glibc )
	valgrind? ( dev-util/valgrind )
	shared-js? ( dev-libs/libffi )
	dbus? (
		>=sys-apps/dbus-0.60
		>=dev-libs/dbus-glib-0.60
	)
	gnome? ( gnome-base/gconf )
	gtk2? ( >=x11-libs/gtk+-2.18.0:2 )
	gtk3? ( >=x11-libs/gtk+-3.4.0:3 )
	media-libs/alsa-lib
	pulseaudio? ( media-sound/pulseaudio )
	media-video/ffmpeg[x264]
	necko-wifi? ( net-wireless/wireless-tools )
	system-cairo? ( >=x11-libs/cairo-1.10 )
	system-compress?
	(
		>=sys-libs/zlib-1.2.3
		app-arch/bzip2
	)
	system-images?
	(
		media-libs/libjpeg-turbo
		media-libs/libpng:*[apng]
	)
	system-libevent? ( dev-libs/libevent )
	system-ffi? ( dev-libs/libffi )
	system-hunspell? ( app-text/hunspell )
	system-nss? ( >=dev-libs/nss-3.52.8 )
	system-pixman? ( >=x11-libs/pixman-0.19.2 )
	system-sqlite? ( >=dev-db/sqlite-3.33.0[secure-delete] )
	system-vpx? ( >=media-libs/libvpx-1.5.0 )"

# python 2.7 is also still needed as the mach build system can't handle python3
BDEPEND="
	${RDEPEND}
	>=sys-devel/autoconf-2.13:2.1
	<=sys-devel/gcc-11.0
	dev-lang/yasm
	app-arch/zip
	dev-lang/python:2.7
"

REQUIRED_USE="
	optimize? ( !debug )
	jemalloc? ( !valgrind )
	^^ ( gtk2 gtk3 )
	necko-wifi? ( dbus )
	official-branding? (
		!system-sqlite !system-cairo !system-pixman !system-libevent !system-vpx
		!system-compress !system-images !system-nss !system-ffi !system-hunspell
	)"
S=${WORKDIR}/pale-moon

src_prepare() {
	eapply_user
	mv ${WORKDIR}/uxp/* ${S}/platform
	mv ${WORKDIR}/uxp/.[a-z]* ${S}/platform
	rmdir ${WORKDIR}/uxp

	default
}

src_configure() {
	# Basic configuration:
	echo "ac_add_options --enable-application=palemoon" > "${S}/.mozconfig"
	echo "ac_add_options --disable-updater" >> "${S}/.mozconfig"
	echo "ac_add_options --disable-install-strip" >> "${S}/.mozconfig"
	echo "ac_add_options --disable-accessibility" >> "${S}/.mozconfig"
	echo "ac_add_options --disable-eme" >> "${S}/.mozconfig"
	echo "ac_add_options --disable-webrtc" >> "${S}/.mozconfig"
	echo "ac_add_options --disable-gamepad" >> "${S}/.mozconfig"
	echo "ac_add_options --disable-tests" >> "${S}/.mozconfig"


	if use official-branding; then
		elog "You are enabling the official branding. You may not redistribute this build"
		elog "to any users on your network or the internet. Doing so puts yourself into"
		elog "a legal problem with Moonchild Productions."
		elog "You can disable the official branding by emerging ${PN} _without_"
		elog "the official-branding USE flag."
		echo "ac_add_options --enable-official-branding" >> "${S}/.mozconfig"
	fi

	# System-* flags
	# Some options aren't in configure.in anymore (e.g. webp, spelling)
	if use system-compress; then
		echo "ac_add_options --with-system-zlib" >> "${S}/.mozconfig"
		echo "ac_add_options --with-system-bz2" >> "${S}/.mozconfig"
	fi

	if use system-libevent; then
		echo "ac_add_options --with-system-libevent" >> "${S}/.mozconfig"
	fi

	if use system-nss; then
		echo "ac_add_options --with-system-nss" >> "${S}/.mozconfig"
	fi

	if use system-images; then
		echo "ac_add_options --with-system-jpeg" >> "${S}/.mozconfig"
		echo "ac_add_options --with-system-png" >> "${S}/.mozconfig"
	fi

	if use system-vpx; then
		echo "ac_add_options --with-system-libvpx" >> "${S}/.mozconfig"
	fi

	if use system-sqlite; then
		echo "ac_add_options --with-system-sqlite" >> "${S}/.mozconfig"
	fi

	if use system-cairo; then
		echo "ac_add_options --with-system-cairo" >> "${S}/.mozconfig"
	fi

	if use system-pixman; then
		echo "ac_add_options --with-system-pixman" >> "${S}/.mozconfig"
	fi

	if use system-ffi; then
		echo "ac_add_options --with-system-ffi" >> "${S}/.mozconfig"
	fi

	if use optimize; then
		O='-O2'
		if use cpu_flags_x86_sse && use cpu_flags_x86_sse2; then
			O="${O} -msse2 -mfpmath=sse"
		fi
		echo "ac_add_options --enable-optimize=\"${O}\"" >> "${S}/.mozconfig"
		filter-flags '-O*' '-msse2' '-mfpmath=sse'
	else
		echo "ac_add_options --disable-optimize" >> "${S}/.mozconfig"
	fi

	if use threads; then
		echo "ac_add_options --with-pthreads" >> "${S}/.mozconfig"
	fi

	if use debug; then
		echo "mk_add_options MOZ_DEBUG_SYMBOLS=1" >> "${S}/.mozconfig"
		echo "ac_add_options --enable-debug-symbols=\"-gdwarf-2\"" >> "${S}/.mozconfig"
	fi

	if ! use shared-js; then
		echo "ac_add_options --disable-shared-js" >> "${S}/.mozconfig"
	fi

	if use jemalloc; then
		echo "ac_add_options --enable-jemalloc" >> "${S}/.mozconfig"
	fi

	if use valgrind; then
		echo "ac_add_options --enable-valgrind" >> "${S}/.mozconfig"
	fi

	if ! use dbus; then
		echo "ac_add_options --disable-dbus" >> "${S}/.mozconfig"
	fi

	if ! use gnome; then
		echo "ac_add_options --disable-gconf" >> "${S}/.mozconfig"
	fi

	if use gtk2; then
		echo "ac_add_options --enable-default-toolkit=\"cairo-gtk2\"" >> "${S}/.mozconfig"
	fi

	if use gtk3; then
		echo "ac_add_options --enable-default-toolkit=\"cairo-gtk3\"" >> "${S}/.mozconfig"
	fi

	if ! use necko-wifi; then
		echo "ac_add_options --disable-necko-wifi" >> "${S}/.mozconfig"
	fi

	if ! use pulseaudio; then
		echo "ac_add_options --disable-pulseaudio" >> "${S}/.mozconfig"
	fi

	if use devtools; then
		echo "ac_add_options --enable-devtools" >> "${S}/.mozconfig"
	fi

	if ! use gconf; then
		echo "ac_add_options --disable-gconf" >> "${S}/.mozconfig"
	fi

	# Enabling this causes xpcshell to hang during the packaging process,
	# so disabling it until the cause can be tracked down. It most likely
	# has something to do with the sandbox since the issue goes away when
	# building with FEATURES="-sandbox -usersandbox".
	echo "ac_add_options --disable-precompiled-startupcache" >> "${S}/.mozconfig"

	# Mainly to prevent system's NSS/NSPR from taking precedence over
	# the built-in ones:
	append-ldflags -Wl,-rpath="$EPREFIX/usr/$(get_libdir)/palemoon"

	export MOZBUILD_STATE_PATH="${WORKDIR}/mach_state"
#	echo "mk_add_options PYTHON=$(which python2)" >> "${S}/.mozconfig"
#	echo "mk_add_options AUTOCONF=$(which autoconf-2.13)" >> "${S}/.mozconfig"
	echo "mk_add_options MOZ_MAKE_FLAGS=\"${MAKEOPTS}\"" >> "${S}/.mozconfig"

	# Shorten obj dir to limit some errors linked to the path size hitting
	# a kernel limit (127 chars):
	echo "mk_add_options MOZ_OBJDIR=\"@TOPSRCDIR@/o\"" >> "${S}/.mozconfig"

	# Disable mach notifications, which also cause sandbox access violations:
	export MOZ_NOSPAM=1
}

src_compile() {
	# Prevents portage from setting its own XARGS which messes with the
	# Pale Moon build system checks:
	# See: https://gitweb.gentoo.org/proj/portage.git/tree/bin/isolated-functions.sh
	export XARGS="$(which xargs)"
	export SHELL="${SHELL:-${EPREFIX}/bin/bash}"
	${S}/mach build || die
}

src_install() {
	# obj_dir changes depending on arch, compiler, etc:
	local obj_dir="$(echo */config.log)"
	obj_dir="${obj_dir%/*}"

	# Disable MPROTECT for startup cache creation:
	pax-mark m "${obj_dir}"/dist/bin/xpcshell

	# Set the backspace behaviour to be consistent with the other platforms:
	echo "pref(\"browser.backspace_action\", 0);" >> "${S}/${obj_dir}/dist/bin/browser/defaults/preferences/palemoon.js"

	# Gotta create the package, unpack it and manually install the files
	# from there not to miss anything (e.g. the statusbar extension):
	einfo "Creating the package..."
	${S}/mach package || die
	local extracted_dir="${T}/package"
	mkdir -p "${extracted_dir}"
	cd "${extracted_dir}"

	einfo "Extracting the package..."
	tar xJpf "${S}/${obj_dir}/dist/${P}.linux-${CTARGET_default%%-*}.tar.xz" || die
	einfo "Installing the package..."
	local dest_libdir="/usr/$(get_libdir)"
	mkdir -p "${D}/${dest_libdir}"
	cp -rL "${PN}" "${D}/${dest_libdir}"
	dosym "${dest_libdir}/${PN}/${PN}" "/usr/bin/${PN}"
	einfo "Done installing the package."

	# Until JIT-less builds are supported,
	# also disable MPROTECT on the main executable:
	pax-mark m "${D}/${dest_libdir}/${PN}/"{palemoon,palemoon-bin,plugin-container}

	# Install icons and .desktop for menu entry:
	cp -rL "${S}/${obj_dir}/dist/branding" "${extracted_dir}/"
	local size sizes icon_path icon name
	sizes="16 32 48"
	icon_path="${extracted_dir}/branding"
	icon="${PN}"
	name="Pale Moon"

	for size in ${sizes}; do
		insinto "/usr/share/icons/hicolor/${size}x${size}/apps"
		 newins "${icon_path}/default${size}.png" "${icon}.png"
	done

	# The 128x128 icon has a different name:
	insinto "/usr/share/icons/hicolor/128x128/apps"
	newins "${icon_path}/mozicon128.png" "${icon}.png"
	# Install a 48x48 icon into /usr/share/pixmaps for legacy DEs:
	newicon "${icon_path}/default48.png" "${icon}.png"
	newmenu "${FILESDIR}/icon/${PN}.desktop" "${PN}.desktop"
	sed -i -e "s:@NAME@:${name}:" -e "s:@ICON@:${icon}:" \
		"${ED}/usr/share/applications/${PN}.desktop" || die
}

pkg_postinst() {
	# Update mimedb for the new .desktop file
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
