# Copyright 2020-2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit distutils-r1

DESCRIPTION="Generate distribution packages from PyPI"
HOMEPAGE="https://github.com/openSUSE/py2pack"
SRC_URI="https://files.pythonhosted.org/packages/source/p/${PN}/${P}.tar.gz"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="*"
IUSE=""

RDEPEND=""

DEPEND="${RDEPEND}
	dev-python/jinja
	dev-python/six
	dev-python/metaextract
	dev-python/pbr
	dev-python/setuptools[${PYTHON_USEDEP}]
	"
