# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1
SRC_URI="https://files.pythonhosted.org/packages/0c/94/23d2062ab57fe78fa886e162c14a70f4c2bbd94961aa762074ed8ca36fb6/${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

DESCRIPTION="Stress-Terminal UI monitoring tool"
HOMEPAGE="https://amanusk.github.io/s-tui"

LICENSE="GPL-2"
SLOT="0"
IUSE="stress"

RDEPEND="
	dev-python/urwid[${PYTHON_USEDEP}]
	dev-python/psutil[${PYTHON_USEDEP}]
	stress? ( app-benchmarks/stress )
"

DEPEND="${RDEPEND}"

BDEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
"
