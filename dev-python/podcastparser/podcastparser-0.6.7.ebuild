# Copyright 2020 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit distutils-r1

DESCRIPTION="Podcast parser for the gpodder client"
HOMEPAGE="https://gpodder.org"
SRC_URI="https://github.com/gpodder/podcastparser/archive/${PV}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND="${PYTHON_DEPS}"
RDEPEND="${DEPEND}"
