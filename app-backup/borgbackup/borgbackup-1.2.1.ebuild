# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit distutils-r1

DESCRIPTION="Deduplicating backup program with compression and authenticated encryption"
HOMEPAGE="https://borgbackup.readthedocs.io/"
SRC_URI="https://gitlab.com/liguros/distfiles/-/raw/main/${P}.tar.gz"

KEYWORDS="amd64 ~arm ~arm64 ~ppc64 x86"
LICENSE="BSD"
SLOT="0"
IUSE="libressl"

RDEPEND="
	app-arch/lz4
	virtual/acl
	dev-python/pyfuse3[${PYTHON_USEDEP}]
	dev-python/msgpack[${PYTHON_USEDEP}]
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
"

DEPEND="
	dev-python/setuptools_scm[${PYTHON_USEDEP}]
	dev-python/packaging[${PYTHON_USEDEP}]
	dev-python/cython[${PYTHON_USEDEP}]
	dev-python/pkgconfig[${PYTHON_USEDEP}]
	dev-python/tox
	dev-python/pytest-xdist
	dev-python/pytest-benchmark
	dev-python/twine
	${RDEPEND}
"

src_install() {
	distutils-r1_src_install
	doman docs/man/*
}
