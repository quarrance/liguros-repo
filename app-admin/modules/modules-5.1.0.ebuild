# Copyright 2020-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="An environment module system supporting TCL and software hierarchy"
HOMEPAGE="http://modules.sourceforge.net/"
SRC_URI="https://github.com/cea-hpc/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 x86"
IUSE="doc test"

DEPEND=">=dev-lang/tcl-8.4.0
		doc? ( >=dev-python/sphinx-1.0.0 )
		test? ( dev-util/dejagnu )"
RDEPEND="${DEPEND}"

LDIR="/etc/Modules"
MDIR="${LDIR}/modules"

src_configure() {
	./configure --prefix=/usr \
				--mandir=/usr/share/man \
				--docdir=/usr/share/doc/${P} \
				--libdir=/usr/lib64 \
				--with-modulepath=${MDIR} \
				--etcdir=${LDIR} \
				--with-initconf-in=etcdir \
				--with-moduleshome=${MDIR}
}

src_install() {
	dodir ${MDIR}
	doins -r "${FILESDIR}/etc"
	default
}

pkg_postinst() {
	elog "You should add the following to your ~/.bashrc to use ${PN}:"
	elog "export MODULEPATH=/usr/modulefiles:${MDIR}"
	elog "[ -f /usr/init/bash ] && \\ "
	elog "    source /usr/init/bash"
	elog ""
	elog "You may also want to run the following commands to update the system cache dir:"
	elog "export MODULEPATH=/usr/modulefiles:${MDIR}"
	elog "source /usr/init/bash"
}
