# Copyright 2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="66-tools - Helpers tools for service scripts administration"
HOMEPAGE="https://framagit.org/Obarun/66-tools"
SRC_URI="https://framagit.org/Obarun/66-tools/-/archive/v${PV}/${PN}-v${PV}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	>=sys-devel/make-3.81
	>=dev-libs/skalibs-2.8.1.0
	>=dev-lang/execline-2.5.1.0
	>=sys-libs/oblibs-0.0.2.0
	>=app-text/scdoc-1.9.4
"

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
	econf --with-sysdeps=/usr/lib/skalibs
}

src_install() {
	emake DESTDIR="${D}" install

	dodoc INSTALL.md NEWS.md README.md AUTHORS
}
