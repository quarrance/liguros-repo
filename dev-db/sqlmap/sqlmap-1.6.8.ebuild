# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="sqlite"

inherit eutils bash-completion-r1 python-single-r1

DESCRIPTION="An automatic SQL injection and database takeover tool"
HOMEPAGE="https://github.com/sqlmapproject/sqlmap"
SRC_URI="https://github.com/sqlmapproject/sqlmap/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64 ~arm64 x86"

LICENSE="GPL-2"
SLOT=0
IUSE="doc mysql psycopg"

RDEPEND="
	${PYTHON_DEPS}
	psycopg? ( dev-python/psycopg )
	mysql? ( dev-python/pymysql )
	dev-python/sqlalchemy
"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

pkg_setup() {
	python-single-r1_pkg_setup
}

src_prepare() {
	mv doc/ "${T}"/doc || die
	default
}

src_install () {
	dodoc -r \
		README.md \
		$(use doc && echo "${T}/doc/*")

	dodir "/usr/share/${PN}/"
	cp -R * "${D}/usr/share/${PN}/" || die
	python_optimize "${D}/usr/share/${PN}"

	make_wrapper $PN \
		"python3 /usr/share/${PN}/sqlmap.py"

	newbashcomp "${FILESDIR}"/sqlmap.bash-completion sqlmap
}
