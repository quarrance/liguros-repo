# Copyright 2020-2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} pypy3 )

inherit bash-completion-r1 distutils-r1

DESCRIPTION="Easy to use progress bars"
HOMEPAGE="https://pypi.org/project/progress/ https://github.com/verigak/progress/"
SRC_URI="https://github.com/verigak/progress/archive/${PV}.tar.gz"

SLOT="0"
LICENSE="ISC"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~amd64-linux ~x86-linux"

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"

# Not bundled
RESTRICT="test"

python_test() {
	"${PYTHON}" test_progress.py || die
}
