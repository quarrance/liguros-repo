# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"
RESTRICT="test"

DISTUTILS_USE_SETUPTOOLS=pyproject.toml
inherit distutils-r1

DESCRIPTION="A tool for authoring Sphinx themes with a simple (opinionated) workflow"
HOMEPAGE="https://pypi.org/project/sphinx-theme-builder/"
SRC_URI="https://files.pythonhosted.org/packages/20/65/5c87d75347c11ef8cbe9b6947d269d3ec6f7e4adffafcbc84f0031e3fa84/sphinx-theme-builder-0.2.0b1.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

DEPEND="
	dev-python/pyproject-metadata
"

S=${WORKDIR}/${PN}-0.2.0b1

distutils_enable_sphinx docs
distutils_enable_tests setup.py

