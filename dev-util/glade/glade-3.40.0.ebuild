# Copyright 2020-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
GNOME2_LA_PUNT="yes"
PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit gnome2 meson python-single-r1 virtualx

DESCRIPTION="A user interface designer for GTK+ and GNOME"
HOMEPAGE="https://glade.gnome.org/"

LICENSE="GPL-2+ FDL-1.1+"
SLOT="3.10/6" # subslot = suffix of libgladeui-2.so
KEYWORDS="~alpha amd64 arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86"

IUSE="debug gjs gladeui gtk_doc +introspection man python webkit"
REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

RDEPEND="
	dev-libs/atk[introspection?]
	>=dev-libs/glib-2.53.2:2
	gjs? ( dev-libs/gjs )
	>=dev-libs/libxml2-2.4.1:2
	x11-libs/cairo:=
	x11-libs/gdk-pixbuf:2[introspection?]
	>=x11-libs/gtk+-3.24.0:3[introspection?]
	x11-libs/pango[introspection?]
	introspection? ( >=dev-libs/gobject-introspection-1.32:= )
	python? (
		${PYTHON_DEPS}
		x11-libs/gtk+:3[introspection]
		$(python_gen_cond_dep '
			>=dev-python/pygobject-3.8:3[${PYTHON_USEDEP}]
		')
	)
	webkit? ( >=net-libs/webkit-gtk-2.28.0:4 )
"
DEPEND="${RDEPEND}
	app-text/docbook-xml-dtd:4.1.2
	dev-libs/libxslt
	>=dev-util/gtk-doc-am-1.13
	>=dev-util/intltool-0.41.0
	dev-util/itstool
	virtual/pkgconfig
"

RESTRICT="test"

PATCHES=(
	"${FILESDIR}/${PN}-doc-version.patch"
)

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_configure() {
	local mesonargs=(
		$(meson_use gladeui)
		$(meson_use gjs)
		$(meson_use gtk_doc)
		$(meson_use debug)
		$(meson_use introspection)
		$(meson_use man)
		$(meson_use python)
		$(meson_use webkit webkit2gtk)
	)
	meson_src_configure
}

src_test() {
	virtx emake check
}

src_install() {
	meson_src_install
}
