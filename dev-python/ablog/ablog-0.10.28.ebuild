# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="ncurses"
RESTRICT="test"

inherit distutils-r1

DESCRIPTION="Sphinx extension that converts any documentation/website project into a blog."
HOMEPAGE="https://ablog.readthedocs.io/"
SRC_URI="https://files.pythonhosted.org/packages/c3/55/f2369c98feb4d7a8a77b084556dece8dad2ec54e7b90661cc29e862d5760/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

distutils_enable_sphinx docs
distutils_enable_tests setup.py

src_prepare() {
	cd ${S}
	# removing test directory, as it is not allowed to be installed
	rm -rf tests
	default
}
